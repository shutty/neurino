#include "ffile.h"


FFile::FFile( string fileName )
:fileName(fileName)
{
	file = NULL;
}

bool FFile::open( Access accessType )
{
	switch(accessType) {
		case ReadOnly:
			file = new fstream(fileName.data(),fstream::in);
			break;
		case WriteOnly:
			file = new fstream(fileName.data(),fstream::out);
			break;
	}
	return opened();
}

std::string FFile::readLine()
{	
	char* s = new char[256];
	file->getline(s, 255);
	string result(s);
	return result;
}

void FFile::writeLine( string data )
{
	file->write(data.data(),data.length());
}

bool FFile::eof()
{
	if (file)
		return file->eof();
	else
		return true;
}

bool FFile::opened()
{
	if (file)
		return file->is_open();
	else
		return false;
}

FFile::~FFile()
{
	if (file) {
		file->close();
		delete file;
	}
}

void FFile::close()
{
	if (file) {
		file->close();
	}
}