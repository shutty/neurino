#include "linker.h"

Linker::Linker()
{
}

Linker::Linker( xml_node* node, FList<Neuron*> neurons )
{
	xml_node xlinks = node->child("inputs");
	for (xml_node inode = xlinks.child("link"); inode; inode = inode.next_sibling("link")) {
		string fromid = inode.attribute("from").value();
		string toid   = inode.attribute("to").value();
		Neuron *nfrom=NULL, *nto=NULL;
		for (int i=0; i<neurons.count();++i) {
			if (neurons.at(i)->id()==fromid)
				nfrom = neurons.at(i);
			if (neurons.at(i)->id()==toid)
				nto = neurons.at(i);
		}
		if ((nto!=NULL) && (nfrom!=NULL) && (nto!=nfrom)) {
			Link* l = new Link(nfrom,nto,&inode);
			links.append(l);
		}
		for (int i=0; i<links.count();++i) {
			if (linksList.find(links.at(i)->from)!=linksList.end()) {
				FList<Link*>* list = linksList.find(links.at(i)->from)->second;
				if (!list->contains(links.at(i)))
					list->append(links.at(i));
			} else {
				std::pair<Neuron*, FList<Link*>* > p(links.at(i)->from,new FList<Link*>);
				p.second->append(links.at(i));
				linksList.insert(p);
			}

			if (linksList.find(links.at(i)->to)!=linksList.end()) {
				FList<Link*>* list = linksList.find(links.at(i)->to)->second;
				if (!list->contains(links.at(i)))
					list->append(links.at(i));
			} else {
				std::pair<Neuron*, FList<Link*>* > p(links.at(i)->to,new FList<Link*>);
				p.second->append(links.at(i));
				linksList.insert(p);
			}

		}
	}
}
Linker::~Linker()
{
	for (int i=0; i<links.count();++i) {
		Link* l = links.at(i);
		delete l;
		l = NULL;
	}
//	FList<Link*> deleteLinks;
	for (std::map<Neuron*,FList<Link*>* >::iterator p=linksList.begin(); p!=linksList.end();++p) {
		FList<Link*>* l = p->second;
// 		for (int i=0; i<l->count();++i){
// 			Link* link = l->at(i);
// 			if (!deleteLinks.contains(link))
// 				deleteLinks.append(link);
// 		}
		delete l;
		l = NULL;
	}
// 	for (int i=0; i<deleteLinks.count();++i)
// 		delete deleteLinks.at(i);
}

void Linker::link( FList<Neuron*> from, FList<Neuron*> to)
{
	for (int i=0;i<from.count();++i) {
		std::pair<Neuron*, FList<Link*>* > p(from.at(i),new FList<Link*>);
		linksList.insert(p);
	}
	for (int i=0;i<to.count();++i) {
		std::pair<Neuron*, FList<Link*>* > p(to.at(i),new FList<Link*>);
		linksList.insert(p);
	}
	for (int i=0; i<from.count();++i) {
		for (int j=0; j<to.count();++j) {
			Link* lnk = new Link(from.at(i),to.at(j));
			linksList[from.at(i)]->append(lnk);
			linksList[to.at(j)]->append(lnk);
			links.append(lnk);
		}
	}
}

FList<Link*>* Linker::linksFrom( Neuron* from )
{
	return linksList[from];
}

FList<Link*>* Linker::linksTo( Neuron* to )
{
	//if (linksList.contains(to))
	//if ((linksList.find(to))!=(linksList.end()))
		return linksList[to];
	//else
	//	return new FList<Link*>;

}

void Linker::linkBias( FList<Neuron*> neurons, Neuron* bias )
{
	for (int i=0; i<neurons.count();++i)  {
// 		qDebug() << neurons.count();
// 		qDebug() << i << linksList.count();
		Link* l = new Link(bias, neurons.at(i));
		linksList[neurons.at(i)]->append(l);
		links.append(l);
	}
}

void Linker::xml( xml_node* parent )
{
	for (int i=0; i<links.count();++i) {
		links.at(i)->xml(parent);
	}
}