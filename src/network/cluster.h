#ifndef CLUSTER_H
#define CLUSTER_H

// Cluster class
// has some nice functions for forward-propagating the network

#include <limits>
#include "nlib_global.h"
#include "neuron.h"
#include "linker.h"
#include "array.h"
#include "activationfunction.h"
#include "elliotfunction.h"
#include "elliotsymmetricfunction.h"
#include "sigmoidfunction.h"
#include "linearfunction.h"
#include "tanhfunction.h"
#include "thresholdfunction.h"

class NLIB_EXPORT Cluster 
{

public:
	Neuron* bias;			//bias neuron for that layer
	FList<Neuron*> neurons;	//list of neurons
	bool isInput;			//Is this cluster an input?
	bool isOutput;			//Is this cluster an output?
	bool isRecurrent;		// Is this a recurrent cluster? Not used by now
	virtual void updateNeurons();	//forward-propagate a layer
	virtual void updateInputs(FList<float> inpData);	//update input layer's inputs
	void reset();			//reset layer to initial random state
	string id();
	void xml(xml_node* parent);
	Linker* inputs;			//Linker class that handles all layer's inputs
	Linker* outputs;		//Linker class that handles all layer's outputs
	//Constructor. Asks for neurons count, activation function, and two bools: isInput and isOutput
	Cluster(int numNeurons, ActivationFunction* func, bool inp = false, bool outp = false);
	Cluster(xml_node* node);
	ActivationFunction* func;
	~Cluster();

private:
	string pid;
};

#endif // CLUSTER_H
