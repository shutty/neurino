#include "cluster.h"

Cluster::Cluster( int numNeurons, ActivationFunction* func, bool inp /*= false*/, bool outp /*= false*/ )
{
	isInput = inp;
	isOutput = outp;
	this->func = func;
	for (int i=0; i<numNeurons; ++i) {
		Neuron* n = new Neuron();
		n->isInput = inp;
		n->isOutput = outp;
		neurons.append(n);
	}
	bias = new Neuron();
	bias->value = 1;
	bias->isInput = true;
	isRecurrent = false;
	inputs = NULL;
	outputs = NULL;
}

Cluster::Cluster( xml_node* node )
{
	pid = node->attribute("id").value();
	isInput = node->attribute("input").as_bool();
	isOutput = node->attribute("output").as_bool();
	isRecurrent = node->attribute("recurrent").as_bool();
	string funcname = node->child("function").attribute("type").value();
	if (funcname == "elliot")
		func = new ElliotFunction();
	if (funcname == "elliotsymmetric")
		func = new ElliotSymmetricFunction();
	if (funcname == "sigmoid")
		func = new SigmoidFunction();
	if (funcname == "tanh")
		func = new TanhFunction();
	if (funcname == "linear")
		func = new LinearFunction();
	if (funcname == "threshold")
		func = new ThresholdFunction();
	bias = new Neuron(node->child("bias").child("neuron"));
	xml_node n = node->child("neurons"); // this will select bookstore, anyStore, Store, etc.
	for (xml_node inode = n.child("neuron"); inode; inode = inode.next_sibling("neuron"))
	{
		Neuron *neur = new Neuron(inode);
		neurons.append(neur);
	}
}

Cluster::~Cluster()
{
	for (int i=0; i<neurons.count();++i) {
		Neuron* n = neurons.at(i);
		delete n;
	}
	if ((!isInput) && (inputs!=NULL)) {
		delete inputs;
		inputs = NULL;
	}
	delete bias;
	//delete func; // FIXME!!!
// 	if ((!isOutput) && (outputs!=NULL)) {
// 		delete outputs;
// 		inputs = NULL;
// 	}
}

void Cluster::updateNeurons()
{
	if (!isInput) {
		FList<Link*>* list;
		float sum;
		Neuron* n;
		Link* l;
		for (int i=0; i<neurons.count(); ++i) {
			n = neurons.at(i);
			list = inputs->linksTo(n);
			sum = 0;
			for (int j=0; j<list->count();++j) {
				l = list->at(j);
				sum+=l->from->value * l->weight;
			}
			n->value = func->value(sum);
			n->derive = func->derive(sum);
		}
	}
}

void Cluster::updateInputs( FList<float> inpData )
{
	if (inpData.count()==neurons.count()) {
		for (int i=0; i<neurons.count();++i) {
			neurons.at(i)->value = inpData.at(i);
		}
	}
}

void Cluster::reset()
{
	for (int i=0; i<neurons.count();++i)
		neurons.at(i)->reset();
	if (!isInput) {
		for (int i=0; i<inputs->links.count();++i)
			inputs->links.at(i)->reset();
	}
	if (!isOutput) {
		for (int i=0; i<outputs->links.count();++i)
			outputs->links.at(i)->reset();
	}

}

std::string Cluster::id()
{
	if (pid.length()==0)
		pid = str(this);
	return pid;
}

void Cluster::xml( xml_node* parent )
{
	xml_node n = parent->append_child();
	n.set_name("cluster");
	n.append_attribute("id").set_value(id().c_str());
	n.append_attribute("bias").set_value(bias->id().c_str());
	n.append_attribute("input").set_value(str(isInput).c_str());
	n.append_attribute("output").set_value(str(isOutput).c_str());
	n.append_attribute("recurrent").set_value(str(isRecurrent).c_str());
	xml_node xneurons = n.append_child();
	xneurons.set_name("neurons");
	for (int i=0; i<neurons.count();++i) {
		neurons.at(i)->xml(&xneurons);
	}
	xml_node xbias = n.append_child();
	xbias.set_name("bias");
	bias->xml(&xbias);

	xml_node xinputs = n.append_child();
	xinputs.set_name("inputs");
	if (!isInput)
		inputs->xml(&xinputs);
// 	xml_node xoutputs = n.append_child();
// 	xoutputs.set_name("outputs");
// 	if (!isOutput)
// 		outputs->xml(&xoutputs);
	func->xml(&n);
}