#include "linkerconvolution.h"

LinkerConvolution::LinkerConvolution(int kernelSize)
	: Linker(), kernelSize(kernelSize)
{
	//
}

LinkerConvolution::~LinkerConvolution()
{

}

void LinkerConvolution::link( FList<Neuron*> from, FList<Neuron*> to )
{
	if (from.count()<to.count())
		cout << "Warning! convolution layer is bigger than input layer!\n";
	for (int i=0;i<from.count();++i) {
		std::pair<Neuron*, FList<Link*>* > p(from.at(i),new FList<Link*>);
		linksList.insert(p);
		//linksList.insert(from.at(i),new FList<Link*>);
	}
	for (int i=0;i<to.count();++i) {
		std::pair<Neuron*, FList<Link*>* > p(to.at(i),new FList<Link*>);
		linksList.insert(p);
		//linksList.insert(to.at(i),new FList<Link*>);
	}
	int step = (kernelSize-1)/2;
	for (int i=0; i<to.count();++i) {
		for (int j=fmax(i-step, 0); j<fmin(i+step,to.count());++j) {
			Link* lnk = new Link(from.at(i), to.at(j));
			linksList[from.at(i)]->append(lnk);
			linksList[to.at(j)]->append(lnk);
		}
	}
}