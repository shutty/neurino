#ifndef LINKER_H
#define LINKER_H

// Basic linker class
// Stores all linkage information for a cluster

#include "nlib_global.h"
#include "neuron.h"
#include <map>
#include "array.h"
#include "link.h"

using namespace pugi;
// link storage class.

class NLIB_EXPORT Linker 
{

public:
	std::map<Neuron*,FList<Link*>* > linksList;		// std::map container for storing sets of neuron-links pairs
	FList<Link*> links;								// all links 
	Linker();
	Linker(xml_node* node, FList<Neuron*> neurons);
	~Linker();
	virtual void link(FList<Neuron*> from, FList<Neuron*> to);	// link groups of neurons
	FList<Link*>* linksFrom(Neuron* from);				// return all links going from neuron to smth
	FList<Link*>* linksTo(Neuron* to);					// return all links going to neuron from smth
	void linkBias(FList<Neuron*> neurons, Neuron* bias);	// internal. link bias neuron
	void xml(xml_node* parent);
	//FList<Neuron*> knownNeurons;


private:
};

#endif // LINKER_H
