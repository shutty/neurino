#ifndef NETWORK_H
#define NETWORK_H

#include <iostream>
/*#include <values.h>*/
#include "nlib_global.h"
#include "linker.h"
#include "cluster.h"
#include "networkdata.h"
#include "minimath.h"
#include "elliotfunction.h"
#include "gene.h"

using namespace pugi;

// typedef struct  
// {
// 	Cluster* from;
// 	Cluster* to;
// } ClusterLink;

class ClusterLink {
public:
	ClusterLink() {
		from = NULL;
		to = NULL;
	}
	ClusterLink(Cluster* from, Cluster* to) 
		:from(from), to(to) {
		//
	}
	Cluster* from;
	Cluster* to;
	void xml(xml_node* parent) {
		xml_node n = parent->append_child();
		n.set_name("clulink");
		n.append_attribute("id").set_value(str(this).c_str());
		n.append_attribute("from").set_value(from->id().c_str());
		n.append_attribute("to").set_value(to->id().c_str());
	}
};



class NLIB_EXPORT Network
{

public:
	Cluster *inputLayer;
	Cluster *outputLayer;
	FList<ClusterLink> clusterLink;
	FList<Cluster*> layers;
	Network();
	Network(Network* net);
	Network(Gene* g, ActivationFunction* f);
	Network(xml_document *doc);
	~Network();
	void addLayer(Cluster* layer);
	void linkLayers(Cluster* from, Cluster* to, Linker* linker);
	FList<Cluster*> getLayerTree(Cluster* from);
	void getLayerTree(Cluster* from, FList<Cluster*>* list);
	FList<float> testSample(FList<float> inputs);
	Sample testSample(Sample* sample);
	float getMSE(NetworkData* data, bool fair = false);
	float getRelativeError(NetworkData* data, bool fair = false);
	float getNRMS(NetworkData* data, bool fair = false);
	void dumpNetworkInfo();
	void reset();
	void save(string fileName);
	xml_document* save();
	void load(xml_document* doc);

private:
	
};

#endif // NETWORK_H
