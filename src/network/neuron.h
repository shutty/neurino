#ifndef NEURON_H
#define NEURON_H

#include "nlib_global.h"
#include "activationfunction.h"
#include "minimath.h"
#include "pugixml.hpp"
#include <ctime>

using namespace std;
using namespace pugi;

class NLIB_EXPORT Neuron
{

public:
	bool isInput;
	bool isOutput;
	float value;
	float derive;
	float valuePending;
	float derivePending;
	float bpSigma;
	//float rpGradient;
	//float rpGradientPrev;
	float rpDelta;
	Neuron(xml_node node);
	Neuron();
	void reset();
 	void xml(xml_node* parent);
	string id();

private:
	string pid;
	
};

#endif // NEURON_H
