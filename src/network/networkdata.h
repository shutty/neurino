#ifndef NETWORKDATA_H
#define NETWORKDATA_H

#include "nlib_global.h"
#include <math.h>
#include <limits>
#include <iostream>
#include <float.h>
#include "array.h"
#include "ffile.h"
#include "minimath.h"

//#define COEFF 10.0f;
using namespace std;

enum ProbenInterval {PROBEN_TRAIN, PROBEN_VALIDATE, PROBEN_TEST};

typedef struct {
	FList<float> inputs;
	FList<float> outputs;
} Sample;
// =======class Sample {
// public:
// 	Sample() {
// 
// 	}
// 	Sample(Sample& other) {
// 		inputs = other.inputs;
// 		outputs = other.outputs;
// 	}
// // 	Sample& operator=(const Sample& other) {
// // 		inputs = other.inputs;
// // 		outputs = other.outputs;
// // 		return *this;
// // 	}
// 	FList<float> inputs;
// 	FList<float> outputs;
// >>>>>>> .theirs} Sample;

// class Sample {
// public:
// 	Sample() {
// 
// 	}
// 	Sample(Sample& other) {
// 		inputs = other.inputs;
// 		outputs = other.outputs;
// 	}
// 	Sample& operator=(Sample& other) {
// 		inputs = other.inputs;
// 		outputs = other.outputs;
// 		return *this;
// 	}
// 	FList<float> inputs;
// 	FList<float> outputs;
// };


class NLIB_EXPORT NetworkData
{

public:
	//enum SquashType {SQUASH_NONE, SQUASH_LINEAR, SQUASH_LINEXP};
	//SquashType type;	//bool squashed;
	//FList<float> inmax;
	//FList<float> inmin;
	//FList<float> outmax;
	//FList<float> outmin;
	FList<Sample> samples;
	void addSample(FList<float> input, FList<float> output);
	//void loadTimeSeries(string fileName, int inputs, int count, int offset);
	//void loadTimeSeries(FList<float> values, int inputs, int outputs);
	//void loadTimeSeriesRebuild(int inputs, int outputs);
	//void squash(SquashType type, float alpha = 1.0f);
	void save(string fileName);
	//Sample* desquash(Sample* sample);
	NetworkData();
	~NetworkData();
	int inputsCount();
	int outputsCount();
	NetworkData cut(int from, int count);
	//void split(int train, int test, int validate);

private:
	//float squashFunc(float value, float alpha);
	//float desquashFunc(float value, float alpha);
	//float tsmax;
	//float tsmin;
	int icnt;
	int ocnt;
	//float squashAlpha;
	//FList<float> tseries;
};

#endif // NETWORKDATA_H
