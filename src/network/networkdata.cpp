#include "networkdata.h"

NetworkData::NetworkData()
{
	icnt = 0;
	ocnt = 0;
//	type = SQUASH_NONE;
}

NetworkData::~NetworkData()
{

}

void NetworkData::addSample( FList<float> input, FList<float> output )
{
	Sample s;
	s.inputs = input;
	s.outputs = output;
	samples.append(s);
}

// void NetworkData::loadTimeSeries( string fileName, int inputs, int count, int offset)
// {
// 	FList<float> data;
// 	FFile f(fileName);
// 	if (f.open(FFile::ReadOnly)) {
// 		while (!f.eof()) {
// 			float num = tofloat(f.readLine());
// 			data.append(num);
// 		}
// 		samples.clear();
// 		for (int i=data.count()-offset-count; i<data.count()-offset; ++i) {
// 			Sample s;
// 			for (int j=i-inputs; j<i; ++j) {
// 				s.inputs.append(data.at(j));
// 			}
// 			s.outputs.append(data.at(i));
// 			samples.append(s);
// 		}
// 	}
// }

int NetworkData::inputsCount()
{
	if ((icnt==0) && (samples.count()>0)) {
		icnt = samples.at(0).inputs.count();
	}
	return icnt;
}

int NetworkData::outputsCount()
{
	if ((ocnt==0) && (samples.count()>0)) {
		ocnt = samples.at(0).outputs.count();
	}
	return ocnt;

}

// void NetworkData::squash(SquashType type, float alpha)
// {
// 	this->type = type;
// 	//linear squash only for now
// 	if ((type == SQUASH_LINEAR) || (type == SQUASH_LINEXP)) {
// 		if (samples.count()>0) {
// 			// min/max initialization
// 			for (int i=0; i<samples[0].inputs.count();++i) {
// 				inmax.append(-99999999.0f);
// 				inmin.append(99999999.0f);
// 			}
// 			for (int i=0; i<samples[0].outputs.count();++i) {
// 				outmax.append(-99999999.0f);
// 				outmin.append(99999999.0f);
// 			}
// 			// finding min/max values for every input vector component
// 			for (int i=0; i<samples.count();++i) {
// 				for (int j=0; j<samples[i].inputs.count();++j) {
// 					if (samples[i].inputs[j]<inmin[j])
// 						inmin[j] = samples[i].inputs[j];
// 					if (samples[i].inputs[j]>inmax[j])
// 						inmax[j] = samples[i].inputs[j];
// 				}
// 				for (int j=0; j<samples[i].outputs.count();++j) {
// 					if (samples[i].outputs[j]<outmin[j])
// 						outmin[j] = samples[i].outputs[j];
// 					if (samples[i].outputs[j]>outmax[j]) {
// 						outmax[j] = samples[i].outputs[j];
// 					}
// 				}
// 			}
// 			// linear squashing
// 			for (int i=0; i<samples.count();++i) {
// 				for (int j=0; j<samples[i].inputs.count();++j) {
// 					float sa = samples[i].inputs[j];
// 					samples[i].inputs[j] = (samples[i].inputs[j] - inmin[j])/(inmax[j] - inmin[j]);
// 					//printf("linearized %f to %f\n",sa, samples[i].inputs[j]);
// 				}
// 				for (int j=0; j<samples[i].outputs.count();++j) {
// 					float sa = samples[i].outputs[j];
// 					samples[i].outputs[j] = (samples[i].outputs[j] - outmin[j])/(outmax[j] - outmin[j]);
// 					//printf("linearized %f to %f\n",sa, samples[i].outputs[j]);
// 				}
// 			}
// 		}
// 	}
// 	if (type==SQUASH_LINEXP) {
// 		squashAlpha = alpha;
// 		for (int i=0; i<samples.count();++i) {
// 			for (int j=0; j<samples[i].inputs.count();++j)
// 				samples[i].inputs[j] = squashFunc(samples[i].inputs[j], alpha);
// 			for (int j=0; j<samples[i].outputs.count();++j)
// 				samples[i].outputs[j] = squashFunc(samples[i].outputs[j], alpha);
// 		}		
// 	}
// }
// 
// Sample* NetworkData::desquash( Sample* sample )
// {
// 	Sample* result = new Sample();
// 	float val;
// 	for (int j=0; j<sample->inputs.count();++j) {
// 		if (type == SQUASH_LINEXP) {
// 			val = desquashFunc(sample->inputs[j],squashAlpha);
// 		} else {
// 			val = sample->inputs[j];
// 		}
// 		result->inputs.append(val*(inmax[j]-inmin[j])+inmin[j]);
// 	}
// 	for (int j=0; j<sample->outputs.count();++j) {
// 		if (type == SQUASH_LINEXP) {
// 			val = desquashFunc(sample->outputs[j],squashAlpha);
// 		} else {
// 			val = sample->outputs[j];
// 		}
// 		result->outputs.append(val*(outmax[j]-outmin[j])+outmin[j]);
// 	}
// 	return result;
// }
// 
// float NetworkData::squashFunc( float value, float alpha )
// {
// 	float res = (2/(1+exp(-alpha*value)))-1;
// 	//printf("squashed %f by %f to %f\n",value, alpha, res);
// 	return res;
// }
// 
// float NetworkData::desquashFunc( float value, float alpha )
// {
// 	return -log(2/(value+1)-1)/alpha;
// }
// 
// void NetworkData::loadTimeSeries(FList<float> values, int inputs, int outputs) 
// {
// 	tseries = values;
// 	loadTimeSeriesRebuild(inputs, outputs);
// }
// 
// void NetworkData::loadTimeSeriesRebuild( int inputs, int outputs )
// {
// 	samples.clear();
// 	for (int i=inputs; i<tseries.count();++i) {
// 		Sample s;
// 		for (int j=i-inputs; j<i; j++) {
// 			s.inputs.append(tseries.at(j));
// 		}
// 		for (int j=i; (j<i+outputs); j++) {
// 			s.outputs.append(tseries.at(j));
// 		}
// 		samples.append(s);
// 	}
// }

// void NetworkData::split( int train, int test, int validate )
// {
// 	samplesTrain.clear();
// 	for (int i=0; i<train; ++i)
// 		samplesTrain.append(samples.at(i));
// 	samplesTest.clear();
// 	for (int i=train; i<train+test; ++i)
// 		samplesTest.append(samples.at(i));
// 	samplesValidate.clear();
// 	for (int i=train+test; i<train+test+validate; ++i)
// 		samplesValidate.append(samples.at(i));
// }
// 
void NetworkData::save( string fileName )
{
	FFile f(fileName);
	if (f.open(FFile::WriteOnly)) {
		for (int i=0; i<samples.count(); i++) {
			string line;
			line.append(str(i));
			for (int j=0; j<samples.at(i).inputs.count();++j)
				line.append("\t"+str(samples.at(i).inputs.at(j)));
			for (int j=0; j<samples.at(i).outputs.count();++j)
				line.append("\t"+str(samples.at(i).outputs.at(j)));
			f.writeLine(line+"\n");
		}
		f.close();
	}
}

NetworkData NetworkData::cut(int from, int count) {
    NetworkData result;
    for (int i=from; i<fmin(samples.count(),from+count); ++i) {
	result.samples.append(samples.at(i));
    }
   return result;
}
