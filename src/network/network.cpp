#include "network.h"

Network::Network()
{

}

Network::Network( Gene* g, ActivationFunction* f)
{
    int linkCount=0;
    FList<Cluster*> clusters;
    for (int i=0; i<g->structure.rows; ++i) {
	Cluster *cl = new Cluster(g->sizes.at(i), f, i==0, i==g->structure.rows-1);
	clusters.append(cl);
	addLayer(cl);
    }
    for (int i=0; i<g->structure.rows; ++i) {
	for (int j=0; j<g->structure.cols; ++j) {
	    if (g->structure.at(i,j)) {
		linkLayers(clusters.at(i), clusters.at(j), new Linker());
		linkCount++;
	    }
	}
    }
    if (linkCount<2) {
	cout << "Warning! Insufficient linkage in network structure!\n";
    }
}

Network::Network(Network *net) {
    load(net->save());
}

void Network::load(xml_document *doc) {
    FList<xml_node> nodes;
    xml_node xcluster = doc->child("network").child("clusters");
    for (xml_node inode = xcluster.child("cluster"); inode; inode = inode.next_sibling("cluster"))
    {
	Cluster *clu = new Cluster(&inode);
	nodes.append(inode);
	layers.append(clu);
    }
    xml_node xnetwork = doc->child("network");
    string inp = xnetwork.attribute("input").value();
    string outp = xnetwork.attribute("output").value();
    for (int i=0; i<layers.count();++i) {
	if (inp == layers.at(i)->id())
	    inputLayer = layers.at(i);
	if (outp == layers.at(i)->id())
	    outputLayer = layers.at(i);
    }
    xml_node xclulink = doc->child("network").child("clulinks");
    for (xml_node inode = xclulink.child("clulink"); inode; inode = inode.next_sibling("clulink")) {
	string sfrom = inode.attribute("from").value();
	string sto = inode.attribute("to").value();
	Cluster *cfrom=NULL, *cto=NULL;
	for (int i=0; i<layers.count();++i) {
	    if (layers.at(i)->id()==sfrom)
		cfrom = layers.at(i);
	    if (layers.at(i)->id()==sto)
		cto = layers.at(i);
	}
	if ((cfrom!=NULL) && (cto!=NULL) && (cfrom!=cto)) {
	    ClusterLink* cl = new ClusterLink(cfrom,cto);
	    clusterLink.append(*cl);
	    FList<Neuron*> knownNeurons;
	    knownNeurons.append(cfrom->neurons);
	    knownNeurons.append(cfrom->bias);
	    knownNeurons.append(cto->neurons);
	    knownNeurons.append(cto->bias);
	    Linker* lnkr = new Linker(&nodes[(layers.index(cto))], knownNeurons);
	    cfrom->outputs = lnkr;
	    cto->inputs = lnkr;
	} else
	    cout << "incosistent cluster linkage\n";
    }
}

Network::Network( xml_document *doc )
{
    load(doc);
}

Network::~Network()
{
    ActivationFunction* f = layers.at(0)->func;
    for (int i=0; i<layers.count();++i) {
	Cluster* c = layers.at(i);
	delete c;
    }
    delete f;
}

void Network::linkLayers( Cluster* from, Cluster* to, Linker* linker)
{
    ClusterLink link(from,to);
    clusterLink.append(link);
    //qDebug() << "[D] linked" << from->neurons.count() << "to" << to->neurons.count();
    from->outputs = linker;
    to->inputs = linker;
    linker->link(from->neurons,to->neurons);
    linker->linkBias(to->neurons, to->bias);
}

void Network::addLayer( Cluster* layer )
{
    layers.append(layer);
    if (layer->isInput)
	inputLayer = layer;
    if (layer->isOutput)
	outputLayer = layer;
}

FList<float> Network::testSample( FList<float> inputs )
{
    FList<Cluster*> layers = getLayerTree(outputLayer);
    FList<float> result;
    inputLayer->updateInputs(inputs);
    for (int i=layers.count()-1; i>=0;--i) {
	layers.at(i)->updateNeurons();
    }
    for (int i=0; i<outputLayer->neurons.count();++i) {
	result.append(outputLayer->neurons.at(i)->value);
	//qDebug() << "VAL: " << outputLayer->neurons.at(i)->value;
    }
    return result;
}

Sample Network::testSample( Sample* sample )
{
    Sample result;// = new Sample();
    result.inputs = sample->inputs;
    result.outputs = testSample(sample->inputs);
    return result;
}

FList<Cluster*> Network::getLayerTree( Cluster* from )
{
    FList<Cluster*> result;
    getLayerTree(from, &result);
    return result;
}

void Network::getLayerTree( Cluster* from, FList<Cluster*>* list )
{
    // 	if (list==NULL)
    // 		list = new QList<Cluster*>();
    list->append(from);
    //qDebug() << "[d] added layer: " << from->neurons.count();
    for (int i=0; i<clusterLink.count();++i) {
	if ((clusterLink.at(i).to == from) && (!list->contains(clusterLink.at(i).from))) {
	    getLayerTree(clusterLink.at(i).from, list);
	}
    }
}

void Network::dumpNetworkInfo()
{
    int linkCount=0;
    int neuronCount=0;
    int biasLinks=0;
    cout << "\nDumping network info:\n";
    cout << "  Total clusters: " << layers.count() << endl;
    //QList<Cluster*> clusters = getLayerTree(outputLayer);
    //qDebug() << "  Total clusters now: " << clusters.count();
    for (int i=0; i<layers.count();++i) {
	int clusterLinks=0;
	neuronCount+=layers.at(i)->neurons.count();
	if (!layers.at(i)->isInput) {
	    biasLinks += layers.at(i)->neurons.count();
	    for (int j=0; j<layers.at(i)->neurons.count();++j) {
		FList<Link*>* links = layers.at(i)->inputs->linksTo(layers.at(i)->neurons.at(j));
		clusterLinks += links->count();
		linkCount += links->count();
	    }
	}
	string status;
	if (layers.at(i)->isInput)
	    status=" is INPUT";
	if (layers.at(i)->isOutput)
	    status=" is OUTPUT";
	cout << "  Cluster " << i << ":" << " neurons: " << layers.at(i)->neurons.count() << " links to: " << clusterLinks << status.data() << endl;
    }
    cout << "  Total neurons: " << neuronCount << endl;
    cout << "  Total links: " << linkCount << endl;
    cout << "  Total bias links: " << biasLinks << endl;
    cout << "  Layer linkage:" << endl;
    std::cout << "   ";
    for (int i=0; i<layers.count();++i)
	std::cout << i;
    std::cout << std::endl;
    for (int i=0; i<layers.count();++i) {
	std::cout << "  " << i;
	for (int j=0; j<layers.count();++j) {
	    bool found = false;
	    for (int k=0; k<clusterLink.count();++k)
		if ((clusterLink.at(k).from == layers.at(i)) && (clusterLink.at(k).to == layers.at(j)))
		    found = true;
	    if ((found) && ((layers.at(i)->isRecurrent) || (layers.at(j)->isRecurrent)))
		std::cout << "R";
	    else if ((found) && !((layers.at(i)->isRecurrent) || (layers.at(j)->isRecurrent)))
		std::cout << "X";
	    else
		std::cout << "-";
	}
	std::cout << std::endl;
    }
}

void Network::reset()
{
    FList<Cluster*> list = getLayerTree(outputLayer);
    for (int i=0; i<list.count();++i)
	list.at(i)->reset();
}

void Network::save(string fileName)
{
    save()->save_file(fileName.c_str(), "    ");
}

xml_document* Network::save()
{
    xml_document* doc = new xml_document();
    doc->append_child(node_comment).set_value("This is a Neurino network savefile");
    doc->append_child().set_name("network");

    xml_node network = doc->child("network");
    network.append_attribute("input").set_value(inputLayer->id().c_str());
    network.append_attribute("output").set_value(outputLayer->id().c_str());

    xml_node clusters = network.append_child();
    clusters.set_name("clusters");
    for (int i=0; i<layers.count();++i) {
	layers.at(i)->xml(&clusters);
    }
    xml_node clulinks = network.append_child();
    clulinks.set_name("clulinks");
    for (int i=0; i<clusterLink.count();++i) {
	clusterLink[i].xml(&clulinks);
    }
    return doc;
}

float Network::getRelativeError( NetworkData* data, bool fair )
{
    // TODO: add fair error computation according to squashing
    float mse = 0;
    int cnt=0;
    FList<Cluster*> clusters = getLayerTree(outputLayer);
    for (int sample=0; sample<data->samples.count();++sample) {
	FList<float> inp = data->samples.at(sample).inputs;
	inputLayer->updateInputs(inp);
	for (int i=clusters.count()-1;i>=0;--i) {
	    clusters.at(i)->updateNeurons();
	}
	for (int neuron=0;neuron<outputLayer->neurons.count();++neuron) {
	    int ocount = data->samples.at(sample).outputs.count();
	    float vn=0, vo=0;
	    //if (!data->squashed) {
	    vn = outputLayer->neurons.at(neuron)->value;
	    vo = data->samples.at(sample).outputs.at(neuron);
	    // 			} else {
	    // 				float omax = data->outmax[neuron];
	    // 				float omin = data->outmin[neuron];
	    // 				float mult = omax-omin;
	    // 				float add = omin;
	    // 				float nout = outputLayer->neurons.at(neuron)->value;
	    // 				float sout = data->samples.at(sample).outputs.at(neuron);
	    // 				vn = nout*mult+add;
	    // 				vo = sout*mult+add;
	    // 			}
	    if (vo !=0) {
		mse+= fabs( (vn - vo) /vo );
		cnt++;
	    }
	}
    }

    mse /= (float)outputLayer->neurons.count()*(float)cnt;
    return mse;
}

float Network::getNRMS( NetworkData* data, bool fair )
{
    // TODO: add fair error computation according to squashing
    float mse = 0;
    float xmax = -FLT_MAX;
    float xmin = FLT_MAX;
    FList<Cluster*> clusters = getLayerTree(outputLayer);
    for (int sample=0; sample<data->samples.count();++sample) {
	FList<float> *inp = &data->samples[sample].inputs;
	inputLayer->updateInputs(*inp);
	for (int i=clusters.count()-1;i>=0;--i) {
	    clusters.at(i)->updateNeurons();
	}
	for (int neuron=0;neuron<outputLayer->neurons.count();++neuron) {
	    float vn = outputLayer->neurons.at(neuron)->value;
	    int ocount = data->samples.at(sample).outputs.count();
	    float vo = data->samples.at(sample).outputs.at(neuron);
	    if (vo>xmax)
		xmax = vo;
	    if (vo<xmin)
		xmin = vo;

	    mse+=(vn - vo) * (vn - vo);
	}
    }
    mse = sqrt(mse/(outputLayer->neurons.count()*data->samples.count()))/(xmax-xmin);
    return mse;
}

float Network::getMSE( NetworkData* data, bool fair )
{
    float mse = 0;
    FList<Cluster*> clusters = getLayerTree(outputLayer);
    float vn=0, vo=0;
    for (int sample=0; sample<data->samples.count();++sample) {
	FList<float> *inp = &data->samples[sample].inputs;
	inputLayer->updateInputs(*inp);
	for (int i=clusters.count()-1;i>=0;--i) {
	    clusters.at(i)->updateNeurons();
	}
	//if ((data->type == NetworkData::SQUASH_NONE) || !fair) {
	for (int neuron=0;neuron<outputLayer->neurons.count();++neuron) {
	    vn = outputLayer->neurons.at(neuron)->value;
	    vo = data->samples.at(sample).outputs.at(neuron);

	    mse+=(vn - vo) * (vn - vo);
	}
	// 		} else {
	// 			Sample original;
	// 			Sample neural;
	// 			for (int i=0; i<inputLayer->neurons.count();++i) {
	// 				original.inputs.append(inputLayer->neurons.at(i)->value);
	// 				neural.inputs.append(inputLayer->neurons.at(i)->value);
	// 			}
	// 			for (int i=0; i<outputLayer->neurons.count();++i) {
	// 				original.outputs.append(data->samples.at(sample).outputs.at(i));
	// 				neural.outputs.append(outputLayer->neurons.at(i)->value);
	// 			}
	// 			Sample* originalDesquashed = data->desquash(&original);
	// 			Sample* neuralDesquashed = data->desquash(&neural);
	// 			for (int i=0; i<outputLayer->neurons.count();++i) {
	// 				vn = neuralDesquashed->outputs.at(i);
	// 				vo = originalDesquashed->outputs.at(i);
	//
	// 				mse += (vn - vo) * (vn - vo);
	// 			}
	// 			delete originalDesquashed;
	// 			delete neuralDesquashed;
	// 		}
    }
    mse /= outputLayer->neurons.count()*data->samples.count();
    return mse;
}
