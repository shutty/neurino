#ifndef LINK_H
#define LINK_H

#include "nlib_global.h"
#include "pugixml.hpp"
#include "neuron.h"


class NLIB_EXPORT Link  {

public:
	Link(Neuron* from, Neuron *to, float weight);;
	Link(Neuron *from, Neuron *to);
	Link(Neuron *from, Neuron* to, xml_node* node);
	void reset();
	Neuron *from;
	Neuron *to;
	float weight;			
	float rpGradient;
	float rpGradientPrev;
	float rpDelta;
	float rpDeltaPrev;
	float rpDeltaW;
	float rpDeltaWPrev;
	void xml(xml_node* parent);
};
#endif // LINK_H
