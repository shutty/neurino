#ifndef NWINIT_H
#define NWINIT_H

#include "nlib_global.h"
#include "array.h"
#include "network.h"
#include "networkdata.h"

class NLIB_EXPORT NWInit
{
public:
	NWInit(Network* net, NetworkData* data);
	~NWInit();
};
#endif // NWINIT_H
