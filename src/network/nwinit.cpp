#include "nwinit.h"

NWInit::NWInit(Network* net, NetworkData* data)
{
	float nmax = -FLT_MAX, nmin = FLT_MAX;
	for (int i=0; i<data->samples.count();++i)
		for (int j=0; j<data->inputsCount();++j) {
			if (data->samples.at(i).inputs.at(j)>nmax)
				nmax = data->samples.at(i).inputs.at(j);
			if (data->samples.at(i).inputs.at(j)<nmin)
				nmin = data->samples.at(i).inputs.at(j);
		}
		for (int i=0; i<net->layers.count();++i)
			if (!net->layers.at(i)->isInput) {
				float numInputs=0, numHidden=0, betta = 0, nmin = 100, nmax = 0;
				for (int j=0; j<net->clusterLink.count();++j)
					if (net->clusterLink.at(j).to == net->layers.at(i))
						numInputs=(float)net->clusterLink.at(j).from->neurons.count();
				numHidden = (float)net->layers.at(i)->neurons.count();
				betta = 0.7f * pow(numHidden, 1.0f/numInputs);
				for (int j=0; j<net->layers.at(i)->inputs->links.count();++j)
					net->layers.at(i)->inputs->links.at(j)->weight = frand(-0.5f, 0.5f);
				for (int j=0; j<net->layers.at(i)->inputs->links.count();++j)
					net->layers.at(i)->inputs->links.at(j)->weight *= betta/(nmax-nmin);
			}
}

NWInit::~NWInit()
{
}
