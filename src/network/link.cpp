#include "link.h"

Link::Link( Neuron* from, Neuron *to, float weight ) : from(from), to(to), weight(weight)
{
	rpDelta = 0.1f;
	rpDeltaW = 0;
	rpDeltaWPrev = 0;
	rpGradient = 0;
	rpGradientPrev = 0;
}

Link::Link( Neuron *from, Neuron *to ) : from(from), to(to)
{
	// weight is set randomly in [0,1]
	this->weight = ((float)(rand() % RAND_MAX)/(float)RAND_MAX)/(float)20;
	rpDelta = 0.1f;
	rpDeltaW = 0;
	rpDeltaWPrev = 0;
	rpGradient = 0;
	rpGradientPrev = 0;
}

Link::Link( Neuron *from, Neuron* to, xml_node* node ) : from(from), to(to	)
{
	weight  = node->attribute("weight").as_float();
	rpGradient = node->attribute("rpGradient").as_float();
	rpDelta  = node->attribute("rpDelta").as_float();
	rpDeltaW = node->attribute("rpDeltaW").as_float();
	rpDeltaWPrev = node->attribute("rpDeltaWPrev").as_float();	
}
void Link::reset()
{
	//reset weight
	this->weight = ((float)(rand() % RAND_MAX)/(float)RAND_MAX)/(float)30;
	rpDelta = 0.1f;
	rpDeltaW = 0;
	rpDeltaWPrev = 0;
	rpGradient = 0;
	rpGradientPrev = 0;
}

void Link::xml( xml_node* parent )
{
	xml_node n = parent->append_child();
	n.set_name("link");
	n.append_attribute("from").set_value(from->id().c_str());
	n.append_attribute("to").set_value(to->id().c_str());
	n.append_attribute("weight").set_value(str(weight).c_str());
	n.append_attribute("rpGradient").set_value(str(rpGradient).c_str());
	n.append_attribute("rpDelta").set_value(str(rpDelta).c_str());
	n.append_attribute("rpDeltaW").set_value(str(rpDeltaW).c_str());
	n.append_attribute("rpDeltaWPrev").set_value(str(rpDeltaWPrev).c_str());
}