#ifndef LINKERCONVOLUTION_H
#define LINKERCONVOLUTION_H

// convolution linker
// Usually used for different pattern-recognition tasks
// Shares a lot of code with common Linker class

#include "nlib_global.h"
#include "neuron.h"
#include "linker.h"

class NLIB_EXPORT LinkerConvolution : public Linker
{
public:
	LinkerConvolution(int kernelSize);
	~LinkerConvolution();
	virtual void link(FList<Neuron*> from, FList<Neuron*> to);

private:
	int kernelSize;
};

#endif // LINKERCONVOLUTION_H
