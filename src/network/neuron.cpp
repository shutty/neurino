#include "neuron.h"

void Neuron::xml(xml_node* parent)
{
	xml_node n = parent->append_child();
	n.set_name("neuron");
	n.append_attribute("id").set_value(id().c_str());
	n.append_attribute("value").set_value(str(value).c_str());
	n.append_attribute("derive").set_value(str(derive).c_str());
	n.append_attribute("valuePending").set_value(str(valuePending).c_str());
	n.append_attribute("derivePending").set_value(str(derivePending).c_str());
	n.append_attribute("bpSigma").set_value(str(bpSigma).c_str());
	//n.append_attribute("bpDelta").set_value(str(bpDelta).c_str());
	//n.append_attribute("rpGradient").set_value(str(rpGradient).c_str());
	//n.append_attribute("rpGradientPrev").set_value(str(rpGradientPrev).c_str());
	n.append_attribute("rpDelta").set_value(str(rpDelta).c_str());
	n.append_attribute("input").set_value(str(isInput).c_str());
	n.append_attribute("output").set_value(str(isOutput).c_str());

}

std::string Neuron::id()
{
	if (pid.length()<1) {
		pid = str(this);
	}
	return pid;
}

Neuron::Neuron( xml_node node )
{
	value			= node.attribute("value").as_float();
	derive			= node.attribute("derive").as_float();
	valuePending	= node.attribute("valuePending").as_float();
	derivePending	= node.attribute("derivePending").as_float();
	bpSigma			= node.attribute("bpSigma").as_float();
	//rpGradient		= node.attribute("rpGradient").as_float();
	//rpGradientPrev	= node.attribute("rpGradientPrev").as_float();
	//bpDelta			= node.attribute("bpDelta").as_float();
	pid				= node.attribute("id").value();
	isInput			= node.attribute("input").as_bool();
	isOutput		= node.attribute("output").as_bool();
}

Neuron::Neuron()
{
	srand((unsigned int)time(NULL)); //ugly hack.
	value = 0;
	derive = 0;
	valuePending = 0;
	derivePending = 0;
	bpSigma = 0;
	//rpGradient = 0;
	//rpGradientPrev = 0;
	//bpDelta = 0;
}

void Neuron::reset()
{
	value = 0;
	derive = 0;
	valuePending = 0;
	derivePending = 0;
	bpSigma = 0;
	rpDelta = 0;
	//rpGradient = 0;
	//rpGradientPrev = 0;
	//bpDelta = 0;
}