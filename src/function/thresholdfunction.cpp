#include "thresholdfunction.h"

ThresholdFunction::ThresholdFunction()
	: ActivationFunction()
{

}

ThresholdFunction::~ThresholdFunction()
{

}

float ThresholdFunction::value( float x )
{
	if (x>=0) {
		return 1;
	} else
		return 0;
}

float ThresholdFunction::derive( float x )
{
	if (x==0)
		return FLT_MAX;
	else
		return 0;
}

void ThresholdFunction::xml( xml_node* parent )
{
	xml_node n = parent->append_child();
	n.set_name("function");
	n.append_attribute("type").set_value("threshold");
}