#ifndef ELLIOTFUNCTION_H
#define ELLIOTFUNCTION_H

// Non-symmetric Elliot function
// fast, but not so precise.

#include <math.h>
#include "activationfunction.h"
#include "nlib_global.h"
#include "minimath.h"
#include "pugixml.hpp"

class NLIB_EXPORT ElliotFunction : public ActivationFunction
{

public:
	ElliotFunction();
	~ElliotFunction();
	virtual float value(float x);
	virtual float derive(float x);
	virtual void  xml(xml_node* parent);

private:
	
};

#endif // ELLIOTFUNCTION_H
