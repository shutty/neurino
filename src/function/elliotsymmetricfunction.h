#ifndef ELLIOTSYMMETRICFUNCTION_H
#define ELLIOTSYMMETRICFUNCTION_H

// Symmetric Elliot function
// fast, but not so precise.

#include <math.h>
#include "activationfunction.h"
#include "nlib_global.h"
#include "minimath.h"
#include "pugixml.hpp"

class NLIB_EXPORT ElliotSymmetricFunction : public ActivationFunction
{

public:
	ElliotSymmetricFunction();
	~ElliotSymmetricFunction();
	virtual float value(float x);
	virtual float derive(float x);
	virtual void  xml(xml_node* parent);

private:
	
};

#endif // ELLIOTSYMMETRICFUNCTION_H
