#include "linearfunction.h"

LinearFunction::LinearFunction()
	: ActivationFunction()
{

}

LinearFunction::~LinearFunction()
{

}

float LinearFunction::value( float x )
{
	return x*STEEPNESS;
}

float LinearFunction::derive( float x )
{
	return STEEPNESS;
}

void LinearFunction::xml( xml_node* parent )
{
	xml_node n = parent->append_child();
	n.set_name("function");
	n.append_attribute("type").set_value("linear");
}