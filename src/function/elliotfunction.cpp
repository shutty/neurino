#include "elliotfunction.h"

ElliotFunction::ElliotFunction()
{

}

ElliotFunction::~ElliotFunction()
{

}

float ElliotFunction::value( float x )
{
	//qDebug() << x;
	return (float)(((x*STEEPNESS) / 2) / (1 + ffabs(x*STEEPNESS)) + 0.5);
}

float ElliotFunction::derive( float x )
{
	return STEEPNESS*1/(2*(1+ffabs(x*STEEPNESS))*(1+ffabs(x*STEEPNESS)));
}

void ElliotFunction::xml( xml_node* parent )
{
	xml_node n = parent->append_child();
	n.set_name("function");
	n.append_attribute("type").set_value("elliot");
}