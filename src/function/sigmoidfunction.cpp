#include "sigmoidfunction.h"

SigmoidFunction::SigmoidFunction()
{

}
SigmoidFunction::~SigmoidFunction()
{

}

float SigmoidFunction::value(float x)
{
	return 1/(1 + exp(-2*STEEPNESS*x));
}

float SigmoidFunction::derive(float x)
{
	return 2*STEEPNESS*value(x)*(1 - value(x));
}

void SigmoidFunction::xml( xml_node* parent )
{
	xml_node n = parent->append_child();
	n.set_name("function");
	n.append_attribute("type").set_value("sigmoid");
}