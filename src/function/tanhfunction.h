#ifndef TANHFUNCTION_H
#define TANHFUNCTION_H

#include <math.h>
#include "activationfunction.h"
#include "nlib_global.h"
#include "pugixml.hpp"

class NLIB_EXPORT TanhFunction : public ActivationFunction
{

public:
	TanhFunction();
	~TanhFunction();
	virtual float value(float x);
	virtual float derive(float x);
	virtual void  xml(xml_node* parent);

private:
	
};

#endif // TANHFUNCTION_H
