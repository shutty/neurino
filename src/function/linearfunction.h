#ifndef LINEARFUNCTION_H
#define LINEARFUNCTION_H

// Linear activation function.
// Very fast and very dumb. Do not use it in most cases.

#include "activationfunction.h"
#include "nlib_global.h"
#include "pugixml.hpp"

class NLIB_EXPORT LinearFunction : public ActivationFunction
{

public:
	LinearFunction();
	~LinearFunction();
	virtual float value(float x);
	virtual float derive(float x);
	virtual void  xml(xml_node* parent);

private:
	
};

#endif // LINEARFUNCTION_H
