#ifndef THRESHOLDFUNCTION_H
#define THRESHOLDFUNCTION_H

#include "activationfunction.h"
#include "nlib_global.h"
#include <limits.h>
#include <math.h>
#include <float.h>

class NLIB_EXPORT ThresholdFunction : public ActivationFunction
{

public:
	ThresholdFunction();
	~ThresholdFunction();
	virtual float value(float x);
	virtual float derive(float x);
	virtual void  xml(xml_node* parent);


private:
	
};

#endif // THRESHOLDFUNCTION_H
