#include "elliotsymmetricfunction.h"

ElliotSymmetricFunction::ElliotSymmetricFunction()
	: ActivationFunction()
{

}

ElliotSymmetricFunction::~ElliotSymmetricFunction()
{

}

float ElliotSymmetricFunction::value( float x )
{
	return (x*STEEPNESS) / (1 + ffabs(x*STEEPNESS));
}

float ElliotSymmetricFunction::derive( float x )
{
	return STEEPNESS/((1+ffabs(x*STEEPNESS))*(1+ffabs(x*STEEPNESS)));
}

void ElliotSymmetricFunction::xml( xml_node* parent )
{
	xml_node n = parent->append_child();
	n.set_name("function");
	n.append_attribute("type").set_value("elliotsymmetric");
}