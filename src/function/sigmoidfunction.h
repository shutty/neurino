#ifndef SIGMOIDFUNCTION_H
#define SIGMOIDFUNCTION_H

#include <math.h>
#include "activationfunction.h"
#include "nlib_global.h"
#include "pugixml.hpp"

class NLIB_EXPORT SigmoidFunction : public ActivationFunction
{

public:
	SigmoidFunction();
	~SigmoidFunction();
	virtual float value(float x);
	virtual float derive(float x);
	virtual void  xml(xml_node* parent);

private:
	
};

#endif // SIGMOIDFUNCTION_H
