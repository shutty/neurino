#include "tanhfunction.h"

TanhFunction::TanhFunction()
{

}

TanhFunction::~TanhFunction()
{

}

float TanhFunction::value( float x )
{
	return tanh(STEEPNESS*x);
}

float TanhFunction::derive( float x )
{
	float y = value(x);
	return STEEPNESS*(1-(y*y));
}

void TanhFunction::xml( xml_node* parent )
{
	xml_node n = parent->append_child();
	n.set_name("function");
	n.append_attribute("type").set_value("tanh");
}