#ifndef ACTIVATIONFUNCTION_H
#define ACTIVATIONFUNCTION_H

// Abstract class for all types of activation functions

#include "nlib_global.h"
#include "pugixml.hpp"

using namespace pugi;


#define STEEPNESS 0.5f
// steepiness - alpha parameter in activation function

class NLIB_EXPORT ActivationFunction
{

public:
	ActivationFunction();
	~ActivationFunction();
	virtual float value(float x) =0; //function value
	virtual float derive(float x) = 0; //function derivative
	virtual void  xml(xml_node* parent) = 0;

private:
	
};

#endif // ACTIVATIONFUNCTION_H
