#ifndef FFILE_H
#define FFILE_H

// Internal fopen-wrapper
// Use fopen() and so on instead

#include <iostream>
#include <fstream>
#include "nlib_global.h"
using namespace std;

class NLIB_EXPORT FFile
{
public:
	// open type: only for reading or writing
	enum Access {ReadOnly, WriteOnly};

	FFile(string fileName);
	bool open(Access accessType);	// open file
	string readLine();				//read current line
	void writeLine(string data);	//write line
	bool eof();						// is file at end?
	bool opened();					// is it opened?
	void close();
	~FFile();
private:
	string fileName;
	fstream* file;
};
#endif // FFILE_H
