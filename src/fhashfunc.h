#ifndef FHASHFUNC_H
#define FHASHFUNC_H

#include <string>
#include <iostream>
#include <fstream>

using namespace std;

inline int hash(void* value) {
	return (int)value;
}

inline int hash(int value) {
	return value;
}

inline int hash(string value) {
	char b1 = value[1*value.size()/4];
	char b2 = value[2*value.size()/4];
	char b3 = value[3*value.size()/4];
	char b4 = value[4*value.size()/4];
	return b1*0xFFFFFF+b2*0xFFFF+b3*0xFF+b4;
}
#endif // FHASHFUNC_H
