
#ifndef minimath_h__
#define minimath_h__

// A small library of hand-written math routines

#include <iostream>
#include <sstream>
#include "array.h"
#include "flist.h"
#include "lrint.h"
using namespace std;


template <typename T>
inline string str(const T& val) {		// convert T to string. Small syntax sugar.
	stringstream oss;
	oss << val;
	return oss.str();
};

inline int toint(string str) {
	return atoi(str.c_str());
}

inline float tofloat(string str) {
	return (float)atof(str.c_str());
}

inline float sign(float val) {			// returns sign of value
	if (val>0)
		return 1;
	else if (val<0)
		return -1;
	else
		return 0;		// yes, that's zero
}

inline int newid() {
	return rand();
}

inline bool probably(float percent) {	// returns true with probability of _percent_
	return ((rand() % 100) <= (percent*100.0f));
};

inline bool linkable(int size, int row, int col) { // small routine for testing corrent linkage
	return (!((row==0) && (col==size-1)) && (col>row));
}

inline string part(string str, char separator, int number) { //ugly code for extracting substring
	int begin = 0, current = 0;
	for (unsigned int i=0; i<str.length();i++) {
		if (((str[i]==separator)||(i==str.length()-1))) {
			if (current == number) {
				int end = i-begin;
				if (i==str.length()-1)
					end++;
				string s = str.substr(begin,end);
				return s;
			} else
				current++;
			begin = i+1;
		}
	}
	return "";
}
template <typename T>
inline float average(FList<T> list) {	//return average value for FList<T>
	float result = 0;
	for (int i=0; i<list.count();i++)
		result += list.at(i);
	return result/list.count();
}

template <typename T>
inline T median(FList<T> list) {
    T result = 0;
    FList<T> sorted = list;
    for (int i=0; i<sorted.count(); ++i)
	for (int j=0; j<sorted.count(); ++j)
	    if (sorted[i]<sorted[j]) {
		T tmp = sorted[i];
		sorted.setAt(i,sorted[j]);
		sorted.setAt(j,tmp);
	    }
    return sorted.at(sorted.count()/2);
}

template <typename T>
inline float dispersion(float average, FList<T> list) {	//returns dispersion for FList<T>
	float sum = 0;
	for (int i=0; i<list.count();i++)
		sum+=(average-list.at(i))*(average-list.at(i));
	sum /= (float)list.count();
	return sqrt(sum);
}

template <typename T>
inline float dispersion(FList<T> list) {//returns dispersion for FList<T>
	float avg = average(list);
	return dispersion(avg, list);
}

template <typename T>
inline T flistmin(FList<T> list) { //returns min value of list
	T fmin = list.at(0);
	for (int i=1; i<list.count();i++)
		if (list.at(i)<fmin) 
			fmin = list.at(i);
	return fmin;
}

template <typename T>
inline T flistmax(FList<T> list) {	//.. max value of list
	T fmax = list.at(0);
	for (int i=1; i<list.count();i++)
		if (list.at(i)>fmax) 
			fmax = list.at(i);
	return fmax;
}

//fast exponential pow for integers
inline int fpow(int base, int exp) {
	int result = 1;
	while (exp)
	{
		if (exp & 1)
			result *= base;
		exp >>= 1;
		base *= base;
	}
	return result;
}

inline float frand(float from, float to) {
	return from+((float)rand()/(float)RAND_MAX)*(to-from);
}

template <typename T>
inline T fmax(T a, T b) {
	return (a>b) ? a : b;
}

template <typename T>
inline T fmin(T a, T b) {
	return (a<b) ? a : b;
}

template <typename T>
inline T ffabs(T x) { // fast abs
	return (x > 0) ? x : -x;
}

#endif // minimath_h__
