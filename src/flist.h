#ifndef Flist_H
#define Flist_H

// Hi-performance dynamic array
// faster up to 5 times compared to std::vector
// no sanity checks, be accurate!

// Allocates memory incrementally by blocks (by default, 12 items)
// 12 - optimal for this network according to tests.

// there is a bug in memory allocation code:
// it does not call any constructors on T class creation.
// Use it very accurately with c++ classes - and be aware of this bug

//I'll plan to rewrite this class soon because of:
//1. Crazy C-style memory allocators
//2. Performance degradation on huge sizeof(T)


#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

// VC compiler complains than I didn't dllexport template. But it's impossible, so shut up!
#pragma warning(disable: 4251)

#define DSIZE 12 
// holy shit!
#define DMULT 2

template <class T>
class FList
{
public:
	T* buffer;
	// Empty constructor
	inline FList() {
		buffer = NULL;
		size = 0;
		allocated = DSIZE;
		buffer = new T [allocated];
	}
	// Copy constructor
	inline FList<T>(const FList<T>& list) {
		size = list.size;
		allocated = list.allocated;
		buffer = new T [allocated];
		for (int i=0; i<size; ++i)
			buffer[i] = list[i];
	}
	//destructor
	inline ~FList() {
		delete [] buffer;
	}
	//appends item to the end of array
	inline void append(T element) {
		if (size+1>=allocated) {
			allocated <<= 1; //fast *2 multiply
			T* buffer2 = new T[allocated];
			for (int i=0; i<size; ++i)
				buffer2[i] = buffer[i];
			delete [] buffer;
			buffer = buffer2;
		}
		buffer[size] = element;
		size++;
	}
	inline FList<T> part(int from, int count) {
		FList<T> result;
		for (int i=from; i<from+count;++i)
			result.append(buffer[i]);
		return result;
	}

	inline void append(FList<T> list) {
		for (int i=0; i<list.count();++i)
			append(list.at(i));
	}
	//item read access. Beware of missing index check
	inline T &at(int index) {
		return buffer[index];
	}
	// item write access.
	inline void setAt(int index, T element) {
		buffer[index] = element;
	}
	// array count
	inline int count() {
		return size;
	}
	// resetting array
	inline void clear() {
		size = 0;
		if (allocated) {
			delete [] buffer;
			//free(buffer);
			allocated = DSIZE;
			buffer = new T [allocated];
		}
	}
	//fast search
	inline bool contains(T element) {
		int i=0;
		while ((element!=buffer[i]) && (i<size))
			i++;
		return (i!=size);
	}
	inline int index(T element) {
		int i=0;
		while ((element!=buffer[i]) && (i<size))
			i++;
		if (i!=size)
			return i;
		else
			return -1;
	}
	// item swap
	inline void swap(int one, int two) {
		T temp = buffer[one];
		buffer[one] = buffer[two];
		buffer[two] = temp;
	}
	FList<T> flip() {
	    FList<T> result(*this);
	    for (int i=0; i<count(); ++i) {
		result.setAt(result.count()-i,at(i));
	    }
	    return result;
	}

	// removing last element. Ugly and fast.
	inline void removeLast() {
		size--;
	}
	// copy-like operator
	inline FList& operator = (const FList& list) {
		if (this == &list) {
			return *this;
		} else {
			if (allocated>0)
				delete [] buffer;
			size = list.size;
			allocated = list.allocated;
			buffer = new T [allocated];
			for (int i=0; i<size; ++i)
				buffer[i] = list[i];
			return *this;
		}
	}
	// item access write operator
	inline T& operator [] (int index) {
		return buffer[index];
	}
	// item access read operator
	inline const T& operator [] (int index) const {
		return buffer[index];
	}
	inline void print() {
		for (int i=0; i<size; ++i)
			cout << buffer[i] << " ";
		cout << endl;
	}
	int size;
	int allocated;

private:
};
#endif // Flist_h__
