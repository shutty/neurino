#include "genetics.h"
#include "timeseriesloader.h"

Genetics::Genetics( NetworkData dataTrain, NetworkData dataTest, Gene* initialGene, int minSize, int maxSize, float alpha, float omega, int avgPopulation, int maxPopulation, int generations, int tests, int maxIterations)
: initialGene(initialGene), dataTrain(dataTrain), dataTest(dataTest), minSize(minSize), maxSize(maxSize), alpha(alpha), omega(omega), avgPopulation(avgPopulation), maxPopulation(maxPopulation), generations(generations), tests(tests), maxIterations(maxIterations)
{
	bestMSE = 100;
	srand((unsigned int)time(NULL));
	geneIndex=0;
	mutateInputs=false;
	dataLoader = NULL;
}

Genetics::Genetics( DataLoader* dataLoader, Gene* initialGene,  int minSize, int maxSize, float alpha, float omega, int avgPopulation, int maxPopulation, int generations, int tests, int maxIterations, bool mutateInputs )
: initialGene(initialGene), dataLoader(dataLoader), minSize(minSize), maxSize(maxSize), alpha(alpha), omega(omega), avgPopulation(avgPopulation), maxPopulation(maxPopulation), generations(generations), tests(tests), maxIterations(maxIterations), mutateInputs(mutateInputs)
{
	bestMSE = 100;
	srand((unsigned int)time(NULL));
	geneIndex=0;
	//dataLoader2 = new TimeseriesLoader("d:/cat-1200.txt");
}

Genetics::~Genetics()
{
// 	for (int i=0; i<population.count();++i) {
// 		Gene* g = population.at(i);
// 		delete g;
// 	}
	//delete dataLoader;
}

void Genetics::mutate( Gene g, float prob )
{
	Gene ng(g);
	int start = 1;
	if (probably(prob))
		if (mutateInputs) 
			ng.sizes[0] = inputsLow + (rand() % (inputsHigh-inputsLow));
	for (int i=1; i<g.sizes.count()-1;++i) {
		if (probably(prob))
			ng.sizes[i] = minSize + (rand() % (maxSize-minSize));
	}
	if (probably(prob))
	    if (coeffLow != coeffHigh)
		ng.coeff = coeffLow + (rand() % (int)(coeffHigh - coeffLow));
	if (probably(prob))
	    if (intervalHigh != intervalLow)
		ng.interval = intervalLow + (rand() % (intervalHigh - intervalLow));
	if (probably(prob))
	    if (testAreaHigh != testAreaLow)
		ng.testArea = testAreaLow + ((float)(rand() % (int)((testAreaHigh - testAreaLow)*100.0f)))/100.0f;
// 	ng->inputs = (g->inputs +inputs_min + (rand() % (inputs_max - inputs_min)))/2;
// 	ng->hidden = (g->hidden + hidden_min + (rand() % (hidden_max - hidden_min)))/2;
// 	ng->trainSamples = (g->trainSamples + gene_trsamples_min + (rand() % (gene_trsamples_max - gene_trsamples_min)))/2;
// 	ng->coeff = (g->coeff + gene_coeff_min + (rand() % (int)((int)100*(gene_coeff_max - gene_coeff_min)))/100)/2;
	population.append(ng);
}


void Genetics::spare( Gene* mom, Gene* dad )
{
	Gene ng(mom);
	int start = 1;
	if (mutateInputs)
		start = 0;
	for (int i=start; i<mom->sizes.count();++i) {
		ng.sizes[i] = (mom->sizes.at(i) + dad->sizes.at(i))/2;
	}
	ng.testArea = (mom->testArea + dad->testArea)/2.0f;
	ng.coeff = (mom->coeff + dad->coeff)/2.0f;
	ng.interval = (mom->interval + dad->interval)/2;
	//ng->inputs = (mom->inputs + dad->inputs)/2;
	//ng->hidden = (mom->hidden + dad->hidden)/2;
// 	ng->trainSamples = (mom->trainSamples + dad->trainSamples)/2;
// 	ng->coeff = (mom->coeff + dad->coeff)/2;
	population.append(ng);
}

void Genetics::test( Gene* g )
{
	//mutateInputs = false;
	float cumulativeMSE = 0;
	if (dataLoader != NULL) {
	    dataLoader->setActiveInterval(g->interval);
	    if (mutateInputs) {
		    dataTrain = dataLoader->load(g->sizes[0],1,DataLoader::LOAD_FIRST_PERCENT,1.0f-g->testArea,DataLoader::SQUASH_TSEXP,g->coeff);
		    dataTest = dataLoader->load(g->sizes[0],1,DataLoader::LOAD_LAST_PERCENT,g->testArea,DataLoader::SQUASH_TSEXP,g->coeff);
	    }
	}
	for (int test=0; test<tests; ++test) {
		//cout << "creating net\n";
		Network net(g, new ElliotFunction());
		RPropLearner rlearn(&net);
		//cout << "creating learner\n";
		int it = 0;
		int itBest = maxIterations/2;
		float mse = 0, bestmse = 1;
		for (int i=0; i<maxIterations; ++i) {
			rlearn.iterate(&dataTrain);
			//cout << "iterate\n";
			mse = net.getMSE(&dataTest);
			//cout << "got mse\n";
			if (mse<bestmse) {
				bestmse = mse;
				g->iterations = i;
			}
		}
		cumulativeMSE+=bestmse;
		//cout << "cleanup\n";
		//delete net;
		//delete dataTrainInner;
		//delete dataTestInner;
		//cout << "iteration done\n";
	}
	cumulativeMSE /= (float)tests;
	g->fitness = cumulativeMSE;
	if (cumulativeMSE< bestMSE) {
		bestMSE = cumulativeMSE;
		bestGene = g;
	}
	geneIndex++;
	switch (g->sizes.count()) {
		case 3:
			printf("\r[GENETIC]\t%3d/%3d [%3d %3d %3d] [%3d %4.2f %4.2f] %6f %6f", geneIndex, currentGeneration, g->sizes[0], g->sizes[1], g->sizes[2], g->interval, g->coeff, g->testArea, cumulativeMSE, bestMSE);
			break;
		case 4:
			printf("\r[GENETIC]\t%3d/%3d [%3d %3d %3d %3d] %6f %6f", geneIndex, currentGeneration, g->sizes[0], g->sizes[1], g->sizes[2], g->sizes[3], cumulativeMSE, bestMSE);
			break;
		case 5:
			printf("\r[GENETIC]\t%3d/%3d [%3d %3d %3d %3d %3d] %6f %6f", geneIndex, currentGeneration, g->sizes[0], g->sizes[1], g->sizes[2], g->sizes[3], g->sizes[4], cumulativeMSE, bestMSE);
			break;
		default:
			printf("\r[GENETIC]\t%3d/%3d [someshit] %6f %6f", geneIndex, currentGeneration, cumulativeMSE, bestMSE);
	}
	if (bestMSE == cumulativeMSE)
		cout << endl;
}

Gene Genetics::populate()
{
	//cout << "initial mutation\n";
	FList<float> avgIterations;
	for (int i=0; i<maxPopulation; ++i) 
		mutate(*initialGene,0.9f);
	for (int i=0; i<generations; ++i) {
	//	cout << "gene loop started\n";
		geneIndex = 0;
		float averageError = 0;
		currentGeneration = i+1;
		int purged = 0;
		// calculating fitness
		for (int j=0; j<population.count();++j) {
			if (population.at(j).fitness==-1) {
	//			cout << "calculation fitness\n";
				test(&population.at(j));
				avgIterations.append((float)population.at(j).iterations);
			}
			averageError += population.at(j).fitness;
		}
		averageError /= population.count();
		// filtering population
		for(int si = population.count() - 1 ; si > 0 ; si--)
			for(int sj = 0 ; sj < si ; sj++)
				if ( population.at(sj).fitness > population.at(sj+1).fitness )
					population.swap(sj,sj+1);
		while (population.count()>avgPopulation) {
			population.removeLast();
			purged++;
		}
		int popc = population.count();
		while (population.count()<maxPopulation) {
			int one = rand() % (int)ceil(popc*alpha);
			int two = rand() % (int)ceil(popc*omega);
			int three = rand() % popc;
			spare(&population.at(one), &population.at(two));
			mutate(population.at(three));
		}
	}
	//avgIterations.printf();
	float it = 0;
	int itcnt = 0;
	for (int i=0; i<avgIterations.count();++i)
		if (avgIterations[i]>30) {
			it+=avgIterations[i];
			itcnt++;
		}
	if (itcnt == 0)
		bestGene->iterations = 70;
	else
		bestGene->iterations = (int)(it/(float)itcnt);
	return *bestGene;
}

void Genetics::setInputs( int low, int high )
{	
	inputsLow = low;
	inputsHigh = high;
}

void Genetics::setInterval( int low, int high )
{
	intervalLow = low;
	intervalHigh = high;
}

void Genetics::setTestArea( float low, float high )
{
	testAreaLow = low;
	testAreaHigh = high;
}

void Genetics::setCoeff( float low, float high )
{
	coeffLow = low;
	coeffHigh = high;
}
