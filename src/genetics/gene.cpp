#include "gene.h"

Gene::Gene( Gene* parent )
{
	this->sizes = parent->sizes;
	Matrix<bool> defStructure(parent->structure.rows, parent->structure.cols, false);
	this->structure = defStructure;
	for (int i=0; i<parent->structure.rows; ++i) {
		for (int j=0; j<parent->structure.cols; ++j) {
			structure.setAt(i,j,parent->structure.at(i,j));
		}
	}
	fitness = -1;
	iterations = 0;
	testArea = parent->testArea;;
	coeff = parent->coeff;
	interval = parent->interval;
}

Gene::Gene(const Gene& parent) {
	this->sizes = parent.sizes;
	//Matrix<bool> defStructure(parent.structure.rows, parent.structure.cols, false);
	this->structure = parent.structure;
	/*for (int i=0; i<parent.structure.rows; ++i) {
		for (int j=0; j<parent.structure.cols; ++j) {
			structure.setAt(i,j,parent.structure.at(i,j));
		}
	}*/
	fitness = -1;
	iterations = 0;
	testArea = parent.testArea;
	coeff = parent.coeff;
	interval = parent.interval;

}

Gene::Gene( FList<int> sizes )
:sizes(sizes)
{
	int numLayers = sizes.count();
	Matrix<bool> defStructure(numLayers, numLayers, false);
	this->structure = defStructure;
	for (int i=0; i<numLayers-1; i++) {
		structure.setAt(i,i+1,true);
	}
	fitness = -1;
}

Gene::Gene()
{
	fitness = -1;
}

Gene& Gene::operator=( const Gene& g )
{
	if (this == &g) {
		return *this;
	} else {
		fitness = g.fitness;
		structure = g.structure;
		sizes = g.sizes;
		iterations = g.iterations;
		testArea = g.testArea;;
		coeff = g.coeff;
		interval = g.interval;
		return *this;
	}
}
