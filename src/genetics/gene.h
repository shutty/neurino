#ifndef GENE_H
#define GENE_H

// Gene class. Used for best structure computation
// Structure is saved as Matrix<bool> 
// Cluster sizes are stored as and FList<int>
// It's just a storage class

#include "nlib_global.h"
#include "matrix.h"
#include <stdio.h>
#include <stdlib.h>

class NLIB_EXPORT Gene {
public:
	Gene(FList<int> sizes);								// ... on layer sizes only
	Gene(Gene* parent);									// ... on another gene
	Gene(const Gene& parent);
	Gene();
	float fitness;				//fitness value for this gene
	Matrix<bool> structure;	//structure
	FList<int> sizes;			//list of cluster sizes
	int iterations;
	float testArea;
	float coeff;
	int interval;
	Gene& operator = (const Gene& g);
};

#endif // GENE_H
