#ifndef GENETIC_H
#define GENETIC_H

//Genetics class
//Used for calculating optimal network structure
//Calculates:
//1. Sizes
//2. Train samples count
//3. Squashing coefficient

#include <time.h>
#include "matrix.h"
#include "elliotfunction.h"
#include "cluster.h"
#include "linker.h"
#include "network.h"
#include "networkdata.h"
#include "rproplearner.h"
#include "gene.h"
#include "networkdata.h"
#include "dataloader.h"


class NLIB_EXPORT Genetics 
{

public:
	FList<Gene> population;
	Genetics(NetworkData dataTrain, NetworkData dataTest, Gene* initialGene, \
		int minSize, int maxSize, float alpha, float omega, int avgPopulation, \
		int maxPopulation, int generations, int tests, int maxIterations);		//constructor
	Genetics(DataLoader* dataLoader, Gene* initialGene, \
		int minSize, int maxSize, float alpha, float omega, int avgPopulation, \
		int maxPopulation, int generations, int tests, int maxIterations, bool mutateInputs);		//constructor

	Genetics();
	void mutate(Gene g, float prob = 0.5f);							//mutate gene
	void spare(Gene* mom, Gene* dad);				//spare two genes
	void test(Gene* g);								//calculate gene fitness
	Gene populate();								//calculate structure
	int iterations;
	~Genetics();

	void setInputs(int low, int high);
	void setInterval(int low, int high);
	void setTestArea(float low, float high);
	void setCoeff(float low, float high);

private:
	DataLoader* dataLoader;
	string fileName;
	float bestMSE;
	float bestDisp;
	Gene* bestGene;
	Gene* initialGene;
	int currentGeneration;
	NetworkData dataTrain;
	NetworkData dataTest;
	int geneIndex;
	int minSize;
	int maxSize;
	float alpha;
	float omega;
	int avgPopulation;
	int maxPopulation;
	int generations;
	int tests;
	int maxIterations;
	bool mutateInputs;

	int hiddenNeuronsLow;
	int hiddenNeuronsHigh;
	float testAreaLow;
	float testAreaHigh;
	float coeffLow;
	float coeffHigh;
	int inputsLow;
	int inputsHigh;
	int intervalLow;
	int intervalHigh;
};

#endif // GENETIC_H