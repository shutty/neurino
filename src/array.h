#ifndef ARRAY_H
#define ARRAY_H

#include <new>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>

#pragma warning(disable: 4251)

template<class T, int Prealloc = 12> class Array;

template <typename T>
inline const T &iqmax(const T &a, const T &b) { if (a < b) return b; return a; }

// Prealloc = 256 by default, specified in qcontainerfwd.h
template<class T, int Prealloc>
class Array
{
public:
    inline explicit Array(int size = 0);

    inline Array(const Array<T, Prealloc> &other)
        : a(Prealloc), s(0), ptr(reinterpret_cast<T *>(array))
    {
        append(other.constData(), other.size());
    }

    inline ~Array() {
        if (ptr != reinterpret_cast<T *>(array))
            free(ptr);
    }
    inline Array<T, Prealloc> &operator=(const Array<T, Prealloc> &other)
    {
        if (this != &other) {
            clear();
            append(other.constData(), other.size());
        }
        return *this;
    }

    inline void removeLast() {
        realloc(s - 1, a);
    }
    inline int size() const { return s; }
    inline int count() const { return s; }
    inline bool isEmpty() const { return (s == 0); }
    inline void resize(int size);
    inline void clear() { resize(0); }

    inline int capacity() const { return a; }
    inline void reserve(int size);

	inline const T &at(int idx) const {
		return ptr[idx];
	}
    inline T &operator[](int idx) {
        return ptr[idx];
    }
    inline const T &operator[](int idx) const {
        return ptr[idx];
    }

    inline void append(const T &t) {
        if (s == a)   // i.e. s != 0
            realloc(s, s<<1);
        const int idx = s++;
        ptr[idx] = t;
    }

	inline void append(const Array<T, Prealloc> &other) {
		for (int i=0; i<other.count(); ++i)
			this->append(other[i]);
	}

    void append(const T *buf, int size);
	
	inline int index(T element) {
		int i=0;
		while ((i<count()) && (ptr[i]!=element)) {
			i++;
		}
		if (i==count())
			return -1;
		else
			return i;
	}
	
	inline void swap(int idx1, int idx2) {
		T tmp = ptr[idx1];
		ptr[idx1] = ptr[idx2];
		ptr[idx2] = tmp;
// 		this[idx1] = this[idx2];
// 		this[idx2] = tmp;
	}

	inline bool contains(T element) {
		return (index(element)!=-1);
	}

	inline void printf() {
		for (int i=0; i<count(); ++i) {
			std::cout << ptr[i] << std::endl;
		}
	}

    inline T *data() { return ptr; }
    inline const T *data() const { return ptr; }
    inline const T * constData() const { return ptr; }

private:
    void realloc(int size, int alloc);

    int a;
    int s;
    T *ptr;
    union {
        // ### Qt 5: Use 'Prealloc * sizeof(T)' as array size
        char array[sizeof(long long int) * (((Prealloc * sizeof(T)) / sizeof(long long int)) + 1)];
        long long int q_for_alignment_1;
        double q_for_alignment_2;
    };
};

template <class T, int Prealloc>
inline Array<T, Prealloc>::Array(int asize)
    : s(asize) {
    if (s > Prealloc) {
        ptr = reinterpret_cast<T *>(malloc(s * sizeof(T)));
        a = s;
    } else {
        ptr = reinterpret_cast<T *>(array);
        a = Prealloc;
    }
//     if (QTypeInfo<T>::isComplex) {
//         T *i = ptr + s;
//         while (i != ptr)
//             new (--i) T;
//     }
}

template <class T, int Prealloc>
inline void Array<T, Prealloc>::resize(int asize)
{ realloc(asize, iqmax(asize, a)); }

template <class T, int Prealloc>
inline void Array<T, Prealloc>::reserve(int asize)
{ if (asize > a) realloc(s, asize); }

template <class T, int Prealloc>
/*inline*/ void Array<T, Prealloc>::append(const T *abuf, int asize)
{
    //Q_ASSERT(abuf);
    if (asize <= 0)
        return;

    const int idx = s;
    const int news = s + asize;
    if (news >= a)
        realloc(s, iqmax(s<<1, news));
    s = news;

//     if (QTypeInfo<T>::isComplex) {
//         T *i = ptr + idx;
//         T *j = i + asize;
//         while (i < j)
//             new (i++) T(*abuf++);
//     } else {
        memcpy(&ptr[idx], abuf, asize * sizeof(T));
//    }
}

template <class T, int Prealloc>
/*inline*/ void Array<T, Prealloc>::realloc(int asize, int aalloc)
{
    //Q_ASSERT(aalloc >= asize);
    T *oldPtr = ptr;
    int osize = s;
    s = asize;

    if (aalloc != a) {
        ptr = reinterpret_cast<T *>(malloc(aalloc * sizeof(T)));
        if (ptr) {
            a = aalloc;

            //if (QTypeInfo<T>::isStatic) {
//                 T *i = ptr + osize;
//                 T *j = oldPtr + osize;
//                 while (i != ptr) {
//                     new (--i) T(*--j);
//                     j->~T();
//                 }
            //} else {
                memcpy(ptr, oldPtr, osize * sizeof(T));
            //}
        } else {
            ptr = oldPtr;
            s = 0;
            asize = 0;
        }
    }

//     if (QTypeInfo<T>::isComplex) {
//         if (asize < osize) {
//             T *i = oldPtr + osize;
//             T *j = oldPtr + asize;
//             while (i-- != j)
//                 i->~T();
//         } else {
//             T *i = ptr + asize;
//             T *j = ptr + osize;
//             while (i != j)
//                 new (--i) T;
//         }
//     }

    if (oldPtr != reinterpret_cast<T *>(array) && oldPtr != ptr)
        free(oldPtr);
}

//QT_END_NAMESPACE

//QT_END_HEADER

#endif // ARRAY_H
