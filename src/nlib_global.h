#ifndef NLIB_GLOBAL_H
#define NLIB_GLOBAL_H

#ifdef _WIN32
#ifdef neurino_EXPORTS
# define NLIB_EXPORT __declspec(dllexport)
//# define NLIB_EXPORT_TEMPLATE
#else
# define NLIB_EXPORT __declspec(dllimport)
//# define NLIB_EXPORT_TEMPLATE extern
#endif
#else
# define NLIB_EXPORT
#endif

#define MAX_NEURONS 1024
#define MAX_CLUSTERS 64

#endif // NLIB_GLOBAL_H
