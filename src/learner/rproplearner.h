#ifndef RPROPLEARNER_H
#define RPROPLEARNER_H


#include "network.h"
#include "learner.h"
#include "nlib_global.h"
#include "networkdata.h"
#include "cluster.h"
#include <math.h>
#include <cmath>
#include <limits>
#include <iostream>
#include <omp.h>

#define DELTAZERO	0.1f
#define DELTAMAX	50.0f
#define DELTAMIN	0.000001f
#define NUMINUS		0.5f
#define NUPLUS		1.2f

enum RPropMode {RPROP, IRPROP, SARPROP, DRPROP, DSARPROP};

class NLIB_EXPORT RPropLearner : public Learner
{

public:
	RPropLearner(Network* net);
	~RPropLearner();
	virtual void learn(NetworkData* data, int iterations = 50000);
	virtual void iterate(NetworkData* data);
	virtual void calcSigmaOutput(Cluster* cluster, FList<float> output);
	virtual void calcSigmaHidden(Cluster* cluster);
	virtual void update(Link* l);
	virtual void init(NetworkData* data);


protected:
	float mse;
	float msePrev;
	int epoch;

private:
	inline void computeGradient(Cluster* cluster);
	bool learnCalled;
	
};

#endif // RPROPLEARNER_H
