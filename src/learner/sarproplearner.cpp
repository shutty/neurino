#include "sarproplearner.h"


SARPropLearner::SARPropLearner( Network* net, float temp )
:RPropLearner(net), t(temp)
{

}

void SARPropLearner::iterate( NetworkData* data )
{
	mse = net->getNRMS(data);
	RPropLearner::iterate(data);
}

void SARPropLearner::update( Link* l )
{
	float mult = l->rpGradient * l->rpGradientPrev;
	if (mult > 0) {
		//l->rpDeltaPrev = l->rpDelta;
		l->rpDelta = fmin(l->rpDelta * NUPLUS, DELTAMAX);
		l->rpDeltaW = /*-*/sign(l->rpGradient) * l->rpDelta;
		l->weight += l->rpDeltaW;
		l->rpGradientPrev = l->rpGradient;
	} else if (mult < 0) {
		if (l->rpDelta < k2*mse*mse) {
			l->rpDelta = l->rpDelta * NUMINUS + k3*frand(0.0f,1.0f)*mse*pow(2.0f,-t*epoch);
		} else {
			l->rpDelta = fmax(l->rpDelta * NUMINUS, DELTAMIN);
		}
		l->rpGradientPrev = 0;
	} else if (mult == 0) {
		l->rpDeltaW = /*-*/sign(l->rpGradient) * l->rpDelta;
		l->weight +=  l->rpDeltaW;
		l->rpGradientPrev = l->rpGradient;
	}
}

SARPropLearner::~SARPropLearner()
{

}

void SARPropLearner::init( NetworkData* data )
{
	RPropLearner::init(data);
	for (int i=0;i<clusters.count();++i) {
		for (int j=0; j<clusters.at(i)->neurons.count();++j) {
			if (!clusters.at(i)->isInput) {
				FList<Link*>* links = clusters.at(i)->inputs->linksTo(clusters.at(i)->neurons.at(j));
				for (int k=0; k<links->count();++k) {
					links->at(k)->rpGradient -= 0 + k1*links->at(k)->weight*pow(2.0f,-t*epoch);
				}
			}
		}
	}

}