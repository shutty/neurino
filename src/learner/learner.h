#ifndef LEARNER_H
#define LEARNER_H

// Abstract learner class
// All learners are children of this class

#include "nlib_global.h"
#include "network.h"
#include "networkdata.h"

class NLIB_EXPORT Learner
{

public:
	Learner(Network* net);
	~Learner();
	virtual void learn(NetworkData* data, int iterations = 50000) = 0;
	virtual void iterate(NetworkData* data) = 0;
	FList<Cluster*> clusters;
	Network* net;


private:
	virtual void calcSigmaOutput(Cluster* cluster, FList<float> output) = 0;
	virtual void calcSigmaHidden(Cluster* cluster)=0;
	
};

#endif // LEARNER_H
