#include "irproplearner.h"

IRPropLearner::IRPropLearner( Network* net )
:RPropLearner(net)
{
	//
}

IRPropLearner::~IRPropLearner()
{

}

void IRPropLearner::iterate( NetworkData* data )
{
	msePrev = mse;
	mse = net->getMSE(data);
	RPropLearner::iterate(data);
}

void IRPropLearner::update( Link* l )
{
	float mult = l->rpGradient * l->rpGradientPrev;
	if (mult > 0) {
		l->rpDelta = fmin(l->rpDelta * NUPLUS, DELTAMAX);
		l->rpDeltaW = /*-*/sign(l->rpGradient) * l->rpDelta;
		l->weight += l->rpDeltaW;
		l->rpGradientPrev = l->rpGradient;
	} else if (mult < 0) {
		l->rpDelta = fmax(l->rpDelta * NUMINUS, DELTAMIN);
		// iRPROP+
		if (mse > msePrev)
			l->weight -= l->rpDeltaW;
		l->rpGradientPrev = 0;
	} else if (mult == 0) {
		l->rpDeltaW = /*-*/sign(l->rpGradient) * l->rpDelta;
		l->weight += l->rpDeltaW;
		l->rpGradientPrev = l->rpGradient;
	}
}