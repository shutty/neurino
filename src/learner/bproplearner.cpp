#include "bproplearner.h"

BPropLearner::BPropLearner(Network* net, bool useMomentum)
	:Learner(net), useMomentum(useMomentum)
{

}

BPropLearner::~BPropLearner()
{
	
}

void BPropLearner::learn(NetworkData* data , int iterations)
{
	float sum = 0, mse = 0;
	for (int epoch=0;epoch<iterations;++epoch) {
		iterate(data);
	}
}

void BPropLearner::calcSigmaOutput( Cluster* cluster, FList<float> output )
{
	float v = 0, dv = 0;
	for (int i=0;i<cluster->neurons.count();++i) {
		v = cluster->neurons.at(i)->value;
		cluster->neurons.at(i)->bpSigma = cluster->neurons.at(i)->derive*(output.at(i)-v);
		//qDebug() << "SIGMA O: " << cluster->neurons.at(i)->sigma;
 	}
}

void BPropLearner::calcSigmaHidden( Cluster* cluster )
{
	float sum = 0;
	FList<Link*>* neurLinks;
	for (int i=0;i<cluster->neurons.count();++i) {
		sum = 0;
		neurLinks = cluster->outputs->linksFrom(cluster->neurons.at(i));
		for (int j=0; j<neurLinks->count(); ++j) {
			sum += neurLinks->at(j)->weight * neurLinks->at(j)->to->bpSigma;
		}
		cluster->neurons.at(i)->bpSigma = cluster->neurons.at(i)->derive*sum;
		//qDebug() << "SIGMA H: " << cluster->neurons.at(i)->sigma;
	}
}

void BPropLearner::updateWeights( Cluster* cluster )
{
	FList<Link*>* links;
	Neuron* n;
	float oldDelta = 0, delta = 0;
	for (int j=0; j<cluster->neurons.count();++j) {
		n = cluster->neurons.at(j);
		links = cluster->inputs->linksTo(n);
		for (int i=0; i<links->count();++i) {
			oldDelta = n->rpDelta;
			n->rpDelta = n->bpSigma * LEARNRATE * links->at(i)->from->value;
			if (useMomentum)
				links->at(i)->weight += n->rpDelta + MOMENTUM*oldDelta;
			else
				links->at(i)->weight += n->rpDelta;
		}
	}
}

void BPropLearner::iterate( NetworkData* data )
{
	for (int j=0;j<data->samples.count();++j) {
		net->inputLayer->updateInputs(data->samples.at(j).inputs);
		// forward propagating
		for (int i=clusters.count()-1;i>=0;--i) {
			clusters.at(i)->updateNeurons();
		}
		// calculating sigma
		for (int i=0;i<clusters.count();++i) {
			if (clusters.at(i)->isOutput) 
				calcSigmaOutput(clusters.at(i), data->samples.at(j).outputs);
			else
				calcSigmaHidden(clusters.at(i));
		}
		// updating weights
		for (int i=0;i<clusters.count();++i) {
			if (!clusters.at(i)->isInput)
				updateWeights(clusters.at(i));
		}
	}
}
