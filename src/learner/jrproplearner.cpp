#include "jrproplearner.h"


JRPropLearner::JRPropLearner( Network* net )
:RPropLearner(net)
{
	q=1;
}

void JRPropLearner::iterate( NetworkData* data )
{
	msePrev = mse;
	mse = net->getMSE(data);
	RPropLearner::init(data);
	if /*(1==1)*/(mse<=msePrev) {
		for (int cluster=0;cluster<clusters.count();++cluster) {
			if (!clusters.at(cluster)->isInput)
				for (int neuron=0;neuron<clusters.at(cluster)->neurons.count();++neuron) {
					FList<Link*>* links = clusters.at(cluster)->inputs->linksTo(clusters.at(cluster)->neurons.at(neuron));
					for (int link=0;link<links->count();++link) {
						Link* l = links->at(link);
						update(l);
					}
				}
		}
		q = 1;
	} else {
		for (int cluster=0;cluster<clusters.count();++cluster) {
			if (!clusters.at(cluster)->isInput)
				for (int neuron=0;neuron<clusters.at(cluster)->neurons.count();++neuron) {
					FList<Link*>* links = clusters.at(cluster)->inputs->linksTo(clusters.at(cluster)->neurons.at(neuron));
					for (int link=0;link<links->count();++link) {
						Link* l = links->at(link);
						float adj = (1/(float)fpow(m,q))*l->rpDeltaWPrev;
						l->weight += adj;
					}
				}
		}
		q++;
	}

}

void JRPropLearner::update( Link* l )
{
	float mult = l->rpGradient * l->rpGradientPrev;
	if (mult > 0) {
		l->rpDelta = fmin(l->rpDelta * (float)NUPLUS, (float)DELTAMAX);
		l->rpDeltaWPrev = l->rpDeltaW;
		l->rpDeltaW = /*-*/sign(l->rpGradient) * l->rpDelta;
		l->weight = l->weight + l->rpDeltaW;
		l->rpGradientPrev = l->rpGradient;
	} else if (mult < 0) {
		l->rpDelta = fmax(l->rpDelta * (float)NUMINUS, (float)DELTAMIN);
		// iRPROP+
// 		if (mse > msePrev)
// 			l->weight = l->weight - l->rpDeltaW;
		l->rpGradientPrev = 0;
	} else if (mult == 0) {
		l->rpDeltaWPrev = l->rpDeltaW;
		l->rpDeltaW = /*-*/sign(l->rpGradient) * l->rpDelta;
		l->weight = l->weight + l->rpDeltaW;
		l->rpGradientPrev = l->rpGradient;
	}
}

JRPropLearner::~JRPropLearner()
{

}