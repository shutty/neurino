#ifndef BPROPLEARNER_H
#define BPROPLEARNER_H

// Classical backpropagation learning class
// It's fast and stupid.
// Use only as a reference.

#include "network.h"
#include "learner.h"
#include "nlib_global.h"
#include "networkdata.h"
#include "cluster.h"

// bprop parameters
// changing learnrate gives better convergence, but can jump over global minimum
#define LEARNRATE 0.6f
// the same
#define MOMENTUM 0.1f

class NLIB_EXPORT BPropLearner : public Learner
{
 

public:
	// constructor class. asks for a network to learn. Uses momentum by defaults
	BPropLearner(Network* net, bool useMomentum = true);
	// the same as for-loop with iterate() inside
	virtual void learn(NetworkData* data, int iterations = 50000);
	// iterate network over some data
	virtual void iterate(NetworkData* data);
	~BPropLearner();

private:
	bool useMomentum;
	// gradient calculator for output layer
	inline void calcSigmaOutput(Cluster* cluster, FList<float> output);
	// gradient calculator for hidden layers
	inline void calcSigmaHidden(Cluster* cluster);
	// weight update routine
	inline void updateWeights(Cluster* layer);
	
};

#endif // BPROPLEARNER_H
