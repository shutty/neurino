#include "rproplearner.h"

RPropLearner::RPropLearner(Network* net)
	: Learner(net)
{
	learnCalled = false;
	epoch = 0;
	mse = 0;
	msePrev = 0;
}

RPropLearner::~RPropLearner()
{

}

void RPropLearner::learn( NetworkData* data, int iterations /*= 50000*/ )
{
	learnCalled = true;
	for (epoch=0;epoch<iterations;++epoch) {
		iterate(data);
	}
}

void RPropLearner::calcSigmaOutput(Cluster* cluster, FList<float> output)
{
	FList<Link*>* links;
	int link=0;
	float v = 0;
	for (int i=0; i<cluster->neurons.count();++i) {
		// possible parallel
		//omp_set_num_threads(2);
//#pragma omp parallel private(v,links,link)
		{
			v = cluster->neurons.at(i)->value;
			cluster->neurons.at(i)->bpSigma = cluster->neurons.at(i)->derive*(output.at(i)-v);
	//		cluster->neurons.at(i)->bpSigma = v*(1-v)*(outputTrain.at(i)-v);
 			links = cluster->inputs->linksTo(cluster->neurons.at(i));
			for (link=0; link<links->count();++link) {
 				links->at(link)->rpGradient += cluster->neurons.at(i)->bpSigma * links->at(link)->from->value; //cluster->neurons.at(i)->value * (1 - cluster->neurons.at(i)->value) * (outputTrain.at(i) - cluster->neurons.at(i)->value);
			}
		}
	}
}

void RPropLearner::calcSigmaHidden(Cluster* cluster)
{
	float sum;
	Neuron* n;
	FList<Link*>* links;
	int link=0;
	FList<Link*>* linksup;
	for (int neuron=0;neuron<cluster->neurons.count();++neuron) {
		// possible parallel
		//omp_set_num_threads(2);
//#pragma omp parallel private(links,n,link,sum,linksup)
		{
			links = cluster->outputs->linksFrom(cluster->neurons.at(neuron));
			sum = 0;
			for (link=0; link<links->count(); ++link) {
				sum += links->at(link)->to->bpSigma * links->at(link)->weight;
			}
			n = cluster->neurons.at(neuron);
			n->bpSigma = n->derive * sum;/* + n->derivePending*sum;*/
			if (!cluster->isInput) {
				linksup = cluster->inputs->linksTo(n);
				for (link=0; link<linksup->count(); ++link) {
					linksup->at(link)->rpGradient += linksup->at(link)->from->value * n->bpSigma;//links->at(link)->from->value * links->at(link)->to->bpSigma;
				}
			}
		}
	}

}

void RPropLearner::iterate( NetworkData* data )
{
	float mult = 0;
	// iRPROP+
//  	msePrev = mse;
//  	mse = net->getMSE(data);
	init(data);
	FList<Link*>* links;
	int link=0;
	int neuron=0;
	for (int cluster=0;cluster<clusters.count();++cluster) {
		//cout << "thnum\n";
		//omp_set_num_threads(omp_get_num_procs());
//#pragma omp parallel private(links,link,l,neuron)
//		{
			//int n = omp_get_num_threads();
			//cout << "thnum:"<<n << endl;

			if (!clusters.at(cluster)->isInput) {
//				omp_set_num_threads(4);
//#pragma omp parallel private(links,link,neuron) 
				//{
				for (neuron=0;neuron<clusters.at(cluster)->neurons.count();++neuron) {
					links = clusters.at(cluster)->inputs->linksTo(clusters.at(cluster)->neurons.at(neuron));
					for (link=0;link<links->count();++link) {
						update(links->at(link));
					}
//				}
			}
		}
	}
	epoch++;
}

void RPropLearner::update( Link* l )
{
	float mult = l->rpGradient * l->rpGradientPrev;
	if (mult > 0) {
		l->rpDelta = fmin(l->rpDelta * NUPLUS, DELTAMAX);
		l->rpDeltaW = /*-*/sign(l->rpGradient) * l->rpDelta;
		l->weight += l->rpDeltaW;
		l->rpGradientPrev = l->rpGradient;
	} else if (mult < 0) {
		l->rpDelta = fmax(l->rpDelta * NUMINUS, DELTAMIN);
		l->rpGradientPrev = 0;
	} else if (mult == 0) {
		l->rpDeltaW = /*-*/sign(l->rpGradient) * l->rpDelta;
		l->weight += l->rpDeltaW;
		l->rpGradientPrev = l->rpGradient;
	}

}

void RPropLearner::init(NetworkData* data)
{
	for (int i=0;i<clusters.count();++i) {
		// possible parallel
		for (int j=0; j<clusters.at(i)->neurons.count();++j) {
			if (!clusters.at(i)->isInput) {
				FList<Link*>* links = clusters.at(i)->inputs->linksTo(clusters.at(i)->neurons.at(j));
				for (int k=0; k<links->count();++k) {
					links->at(k)->rpGradientPrev = links->at(k)->rpGradient;
					links->at(k)->rpGradient = 0;
				}
			}
		}

	}
	for (int j=0;j<data->samples.count();++j) {
		net->inputLayer->updateInputs(data->samples.at(j).inputs);
		// forward propagating
		for (int i=clusters.count()-1;i>=0;--i) {
			clusters.at(i)->updateNeurons();
		}
		for (int i=0;i<clusters.count();++i) {
			if (clusters.at(i)->isOutput) 
				calcSigmaOutput(clusters.at(i), data->samples.at(j).outputs);
			else
				calcSigmaHidden(clusters.at(i));
		}
	}
	if (epoch==0) {
		for (int i=0;i<clusters.count();++i) {
			// possible parallel
			for (int j=0; j<clusters.at(i)->neurons.count();++j) {
				if (!clusters.at(i)->isInput) {
					FList<Link*>* links = clusters.at(i)->inputs->linksTo(clusters.at(i)->neurons.at(j));
					for (int k=0; k<links->count();++k) {
						links->at(k)->rpGradientPrev = links->at(k)->rpGradient;
					}
				}
			}
		}
		if (!learnCalled)
			epoch++;
	}
}