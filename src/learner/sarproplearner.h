#pragma once
#include "rproplearner.h"

static float k1 = 0.01f;
static float k2 = 0.1f;
static float k3 = 3.0f;

class NLIB_EXPORT SARPropLearner : public RPropLearner
{
public:
	SARPropLearner(Network* net, float temp);
	virtual void iterate(NetworkData* data);
	virtual void update(Link* l);
	virtual void init(NetworkData* data);
	~SARPropLearner();
protected:
	float t;
};
