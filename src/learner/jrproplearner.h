#pragma once
#include "rproplearner.h"

// Jacobi-bisection based version of RPROP
// Not working at all

static int m = 5;

class NLIB_EXPORT JRPropLearner : public RPropLearner
{
public:
	JRPropLearner(Network* net);
	virtual void iterate(NetworkData* data);
	virtual void update(Link* l);
	~JRPropLearner();

private:
	int q;
};
