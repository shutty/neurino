#ifndef IRPROP_H
#define IRPROP_H

//Improved RPROP learner class
//Shares a lot of code with RPropLearner
// A bit precise, a bit slower.
//

#include "rproplearner.h"

class NLIB_EXPORT IRPropLearner : public RPropLearner
{
public:
	IRPropLearner(Network* net);
	virtual void iterate(NetworkData* data);
	virtual void update(Link* l);
	~IRPropLearner();
};
#endif // IRPROP_H
