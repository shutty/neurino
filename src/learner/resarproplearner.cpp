#include "resarproplearner.h"



ReSARPropLearner::ReSARPropLearner( Network* net )
:SARPropLearner(net,0.05f)
{

}

void ReSARPropLearner::iterate( NetworkData* data )
{
	mse = net->getNRMS(data);
	errors.append(mse);
// 	if (errors.count()>30) {
// 		int from = lrint(errors.count()*0.8f);
// 		int cnt = errors.count()-from;
// 		Array<float> errlast = errors.part(from,cnt);
// 	}
	RPropLearner::iterate(data);
}

ReSARPropLearner::~ReSARPropLearner()
{

}

float ReSARPropLearner::explist( float start, int index )
{
	return 3*(-(start/2)+start/(1/(1+exp((-1.0f/(float)index)))));
}