#pragma once
#include "sarproplearner.h"

// not working!

class ReSARPropLearner : public SARPropLearner
{
public:
	ReSARPropLearner(Network* net);
	virtual void iterate(NetworkData* data);
	~ReSARPropLearner();
private:
	FList<float> errors;
	inline float explist(float start, int index);
};
