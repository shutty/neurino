#include "probenloader.h"

ProbenLoader::ProbenLoader( string fileName )
: DataLoader(fileName)
{
	interval = PROBEN_TRAIN;
}
ProbenLoader::~ProbenLoader()
{
}

NetworkData ProbenLoader::load( int numInputs /*= 0 */, int numOutputs /*= 0 */,DataLoader::LoadType ltype, float range, DataLoader::SquashType type, float alpha )
{
	NetworkData result;
	FFile f(fileName);
	if (f.open(FFile::ReadOnly)) {
		int start=0;
		int finish=0;
		int currentLine=0;
		int bool_in = toint(part(f.readLine(), '=',1));
		int real_in = toint(part(f.readLine(), '=',1));
		int bool_out = toint(part(f.readLine(), '=',1));
		int real_out = toint(part(f.readLine(), '=',1));
		int training_examples = toint(part(f.readLine(), '=',1));
		int validation_examples = toint(part(f.readLine(), '=',1));
		int test_examples = toint(part(f.readLine(), '=',1));
		int totalInputs = bool_in + real_in;
		int totalOutputs = bool_out+ real_out;
		switch (interval) {
			case PROBEN_TRAIN:
				start=0;
				finish=training_examples;
				break;
			case PROBEN_VALIDATE:
				start=training_examples;
				finish=training_examples+validation_examples;
				break;
			case PROBEN_TEST:
				start=training_examples+validation_examples;
				finish=training_examples+validation_examples+test_examples;
				break;
		}
		while (!f.eof() && (currentLine<finish)) {
			string data = f.readLine();
			if ((currentLine>=start) && (currentLine<finish)) {
				Sample sample;
				for (int i=0; i<totalInputs; ++i)
					sample.inputs.append(tofloat(part(data,' ',i)));
				for (int i=totalInputs; i<totalInputs+totalOutputs; ++i)
					sample.outputs.append(tofloat(part(data,' ',i)));
				result.samples.append(sample);
			}
			currentLine++;
		}
	} else {
	    printf("Cannot open file: %s\n",fileName.c_str());
	    exit(1);
	}
	return result;
}

void ProbenLoader::setLoadingInterval( ProbenLoader::ProbenInterval interval )
{
	this->interval = interval;
}
