#include "dataloader.h"

DataLoader::DataLoader( string fileName )
:fileName(fileName)
{
	// some shit here
	minone = FLT_MAX;
	maxone = -FLT_MAX;
}
DataLoader::~DataLoader()
{
}

void DataLoader::squash( SquashType type, float alpha /*= 1.0f*/ )
{
	this->reset();
	this->type = type;
	if (type==SQUASH_TSEXP) {
	//	cout << "squashing tsexp\n";
		for (int i=0; i<rawSamples.count();++i) {
			for (int j=0; j<rawSamples.at(i).inputs.count();++j) {
				if (rawSamples.at(i).inputs[j]>maxone)
					maxone = rawSamples.at(i).inputs[j];
				if (rawSamples.at(i).inputs[j]<minone)
					minone = rawSamples.at(i).inputs[j];
			}
			for (int j=0; j<rawSamples.at(i).outputs.count();++j) {
				if (rawSamples.at(i).outputs[j]>maxone)
					maxone = rawSamples.at(i).outputs[j];
				if (rawSamples.at(i).outputs[j]<minone)
					minone = rawSamples.at(i).outputs[j];
			}
		}
		for (int i=0; i<rawSamples.count();++i) {
			for (int j=0; j<rawSamples.at(i).inputs.count();++j)
				rawSamples[i].inputs[j] = (rawSamples.at(i).inputs[j] - minone)/(maxone - minone);
			for (int j=0; j<rawSamples.at(i).outputs.count();++j)
				rawSamples[i].outputs[j] = (rawSamples.at(i).outputs[j] - minone)/(maxone - minone);
		}
	}
	//linear squash only for now
	if ((type == SQUASH_LINEAR) || (type == SQUASH_LINEXP)) {
	//	cout << "squashing linear/linexp\n";
		if (rawSamples.count()>0) {
			// min/max initialization
			for (int i=0; i<rawSamples.at(0).inputs.count();++i) {
				inmax.append(-99999999.0f);
				inmin.append(99999999.0f);
			}
			for (int i=0; i<rawSamples.at(0).outputs.count();++i) {
				outmax.append(-99999999.0f);
				outmin.append(99999999.0f);
			}
			// finding min/max values for every input vector component
			for (int i=0; i<rawSamples.count();++i) {
				for (int j=0; j<rawSamples.at(i).inputs.count();++j) {
					if (rawSamples.at(i).inputs[j]<inmin[j])
						inmin[j] = rawSamples.at(i).inputs[j];
					if (rawSamples.at(i).inputs[j]>inmax[j])
						inmax[j] = rawSamples.at(i).inputs[j];
				}
				for (int j=0; j<rawSamples.at(i).outputs.count();++j) {
					if (rawSamples.at(i).outputs[j]<outmin[j])
						outmin[j] = rawSamples.at(i).outputs[j];
					if (rawSamples.at(i).outputs[j]>outmax[j]) {
						outmax[j] = rawSamples.at(i).outputs[j];
					}
				}
			}
			// linear squashing
			for (int i=0; i<rawSamples.count();++i) {
				for (int j=0; j<rawSamples.at(i).inputs.count();++j) {
					float sa = rawSamples.at(i).inputs[j];
					rawSamples[i].inputs[j] = (rawSamples.at(i).inputs[j] - inmin[j])/(inmax[j] - inmin[j]);
					//printf("linearized %f to %f\n",sa, samples[i].inputs[j]);
				}
				for (int j=0; j<rawSamples.at(i).outputs.count();++j) {
					float sa = rawSamples.at(i).outputs[j];
					rawSamples[i].outputs[j] = (rawSamples.at(i).outputs[j] - outmin[j])/(outmax[j] - outmin[j]);
					//printf("linearized %f to %f\n",sa, samples[i].outputs[j]);
				}
			}
		}
	}
	if ((type==SQUASH_LINEXP) || (type==SQUASH_TSEXP))  {
	//	cout << "squashing linexp/tsexp\n";
		squashAlpha = alpha;
		for (int i=0; i<rawSamples.count();++i) {
			for (int j=0; j<rawSamples.at(i).inputs.count();++j) {
				rawSamples[i].inputs[j] = squashFunc(rawSamples.at(i).inputs[j], alpha);

			}
			for (int j=0; j<rawSamples.at(i).outputs.count();++j) {
				rawSamples[i].outputs[j] = squashFunc(rawSamples.at(i).outputs[j], alpha);
			}
		}		
	}
	//cout << "squashing done!\n";
}

float DataLoader::squashFunc( float value, float alpha )
{
	float res = (2/(1+exp(-alpha*value)))-1;
	//printf("squashed %f by %f to %f\n",value, alpha, res);
	return res;

}

float DataLoader::desquashFunc( float value, float alpha )
{
	return -log(2/(value+1)-1)/alpha;
}

Sample DataLoader::desquash( Sample sample )
{
	if (type != SQUASH_NONE) {
		Sample result;// = new Sample();
		float val;
		for (int j=0; j<sample.inputs.count();++j) {
			if ((type == SQUASH_LINEXP) || (type==SQUASH_TSEXP)) {
				val = desquashFunc(sample.inputs[j],squashAlpha);
			} else {
				val = sample.inputs[j];
			}
			if (type == SQUASH_TSEXP)
				result.inputs.append(val*(maxone - minone)+minone);
			else
				result.inputs.append(val*(inmax[j]-inmin[j])+inmin[j]);
		}
		for (int j=0; j<sample.outputs.count();++j) {
			if ((type == SQUASH_LINEXP) || (type==SQUASH_TSEXP)) {
				val = desquashFunc(sample.outputs[j],squashAlpha);
			} else {
				val = sample.outputs[j];
			}
			if (type == SQUASH_TSEXP)
				result.outputs.append(val*(maxone - minone)+minone);
			else
				result.outputs.append(val*(outmax[j]-outmin[j])+outmin[j]);
		}
		return result;
	} else {
		return sample;
	}

}

void DataLoader::reset()
{
	squashAlpha = 1.0f;
	type = SQUASH_NONE;
	inmax.clear();
	inmin.clear();
	outmax.clear();
	outmin.clear();
	minone = FLT_MAX;
	maxone = -FLT_MAX;
}

void DataLoader::setActiveInterval(int count) {
//blah
}