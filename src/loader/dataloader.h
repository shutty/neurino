#ifndef DATALOADER_H
#define DATALOADER_H

#include "nlib_global.h"
#include "array.h"
#include "networkdata.h"
#include <string>

class NLIB_EXPORT DataLoader
{
public:
	enum SquashType {SQUASH_NONE, SQUASH_LINEAR, SQUASH_LINEXP, SQUASH_TSEXP};
	enum LoadType {LOAD_FIRST_PERCENT, LOAD_LAST_PERCENT, LOAD_ALL, LOAD_NONE, LOAD_FIRST_COUNT, LOAD_LAST_COUNT};
	DataLoader(string fileName);
	~DataLoader();
	virtual NetworkData load(int numInputs = 0, int numOutputs = 0, DataLoader::LoadType ltype = LOAD_ALL, float range = 1.0f, DataLoader::SquashType type = SQUASH_NONE, float alpha = 1.0f) = 0;
	Sample desquash(Sample sample);
	virtual void setActiveInterval(int count);
protected:
	string fileName;
	FList<Sample> rawSamples;
	float squashAlpha;
	SquashType type;
	FList<float> inmax;
	FList<float> inmin;
	FList<float> outmax;
	FList<float> outmin;
	float minone;
	float maxone;
	void squash(SquashType type, float alpha = 1.0f);
	float squashFunc(float value, float alpha);
	float desquashFunc(float value, float alpha);
private:
	void reset();

};
#endif // DATALOADER_H
