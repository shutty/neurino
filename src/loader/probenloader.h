#ifndef PROBENLOADER_H
#define PROBENLOADER_H


#include "dataloader.h"
#include "nlib_global.h"

class NLIB_EXPORT ProbenLoader : public DataLoader
{
public:
	enum ProbenInterval {PROBEN_TRAIN, PROBEN_VALIDATE, PROBEN_TEST};
	ProbenLoader(string fileName);
	~ProbenLoader();
	void setLoadingInterval(ProbenLoader::ProbenInterval interval);
	virtual NetworkData load(int numInputs = 0 , int numOutputs = 0, DataLoader::LoadType ltype = LOAD_ALL, float range = 1.0f, DataLoader::SquashType type = SQUASH_NONE, float alpha = 1.0f );
private:
	ProbenInterval interval;
};
#endif // PROBENLOADER_H
