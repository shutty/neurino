#ifndef FANNLOADER_H
#define FANNLOADER_H

#include "dataloader.h"
#include "nlib_global.h"
#include "ffile.h"

class NLIB_EXPORT FannLoader :	public DataLoader
{
public:
	FannLoader(string fileName);
	~FannLoader();
	virtual NetworkData load(int numInputs = 0 , int numOutputs = 0, LoadType ltype = LOAD_ALL, float range = 1.0f, SquashType type = SQUASH_NONE, float alpha = 1.0f );
	virtual void setActiveInterval(int count);
};
#endif // FANNLOADER_H
