#include "fannloader.h"


FannLoader::FannLoader( string fileName )
:DataLoader(fileName)
{
	// some shit here
}

FannLoader::~FannLoader()
{

}

NetworkData FannLoader::load(int numInputs /*= 0 */, int numOutputs /*= 0 */, LoadType ltype, float range,  SquashType type, float alpha)
{
	this->type = type;
	NetworkData result;
	FFile dataFile(fileName);
	if (dataFile.open(FFile::ReadOnly)) {
		//QTextStream dataStream(&dataFile);
		string params = dataFile.readLine();
		//QStringList paramsSplit = params.split(" ");
		int samplesNumber = toint(part(params, ' ', 0));//paramsSplit.at(0).toInt();
		int inputsNumber = toint(part(params, ' ', 1));//paramsSplit.at(1).toInt();
		int outputsNumber = toint(part(params, ' ', 2));//paramsSplit.at(2).toInt();
		for (int i=0; i<samplesNumber;++i) {
			Sample sample;
			string sinputs = dataFile.readLine();
			string soutputs = dataFile.readLine();
			//QStringList inputs = dataStream.readLine().split(" ");
			//QStringList outputs = dataStream.readLine().split(" ");
			for (int j=0;j<inputsNumber;++j) {
				sample.inputs.append(tofloat(part(sinputs,' ',j)));
				//sample.inputs.append(inputs.at(j).toDouble());
			}
			for (int j=0;j<outputsNumber;++j) 
				sample.outputs.append(tofloat(part(soutputs,' ',j)));
			//				sample.outputs.append(outputs.at(j).toDouble());
			rawSamples.append(sample);
			//result.samples.append(sample);
			//printf("Sample: %6f, %6f, %6f, %6f, %6f, %6f = %6f\n",sample.inputs[0], sample.inputs[1], sample.inputs[2], sample.inputs[3], sample.inputs[4], sample.inputs[5], sample.outputs[0] );
		}
	} else {
		cout << "cannot loadFannData :" << fileName.data() << endl;
	}
	this->squash(type, alpha);
	for (int i=0; i<rawSamples.count(); ++i)
		result.samples.append(rawSamples[i]);
	return result;
}

void FannLoader::setActiveInterval( int count )
{
	//do nothing for now
}