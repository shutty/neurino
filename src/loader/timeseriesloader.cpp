#include "timeseriesloader.h"

TimeseriesLoader::TimeseriesLoader( string fileName )
:DataLoader(fileName)
{
	FFile f(fileName);
	if (f.open(FFile::ReadOnly)) {
		while (!f.eof()) {
			string line = f.readLine();
			if (line.size() > 0) {
				float num = tofloat(line);
				data.append(num);
			}
		}
		f.close();
	}
}

TimeseriesLoader::TimeseriesLoader( FList<float> data )
:DataLoader(""), data(data)
{
	
}

TimeseriesLoader::~TimeseriesLoader()
{

}

NetworkData TimeseriesLoader::load(int numInputs /*= 0*/, int numOutputs /*= 0*/, LoadType ltype, float range, SquashType type, float alpha)
{
	NetworkData result;
	this->type = type;
	//cout << "filling raw samples\n";
	rawSamples.clear();
	for (int i=numInputs; i<data.count();++i) { //here goes the bug if numOutputs>1
		Sample s;
		for (int j=i-numInputs; j<i; j++) {
			s.inputs.append(data.at(j));
		}
		for (int j=i; ((j<i+numOutputs) && (j<data.count())); j++) {
			s.outputs.append(data.at(j));
		}
		rawSamples.append(s);
	}
	//cout << "squashing\n";
	squash(type,alpha);
	//cout << "squash done\n";
	//result.samples.memdebug = true;
	switch (ltype) {
		case LOAD_ALL: 
			for (int i=0; i<rawSamples.count();++i)
				result.samples.append(rawSamples.at(i));
			break;

		case LOAD_FIRST_PERCENT:
			//printf("dumping first percent: from 0 to %d in %d\n", (int)((float)rawSamples->count()*range), rawSamples->count());
			for (int i=0; i<(int)((float)rawSamples.count()*range); ++i) {
				result.samples.append(rawSamples.at(i));
			}
			break;

		case LOAD_LAST_PERCENT:
			//printf("dumping last percent: from %d to %d in %d\n", rawSamples->count()-(int)((float)rawSamples->count()*range), rawSamples->count(), rawSamples->count());
			for (int i=rawSamples.count()-(int)((float)rawSamples.count()*range); i<rawSamples.count(); ++i) {
				//printf("%d :  s=%d  a=%d\n",i, result->samples.size, result->samples.allocated);
				result.samples.append(rawSamples.at(i));
			}
			break;
		case LOAD_FIRST_COUNT:
			for (int i=0; i<(int)range; ++i)
				result.samples.append(rawSamples.at(i));
			break;
		case LOAD_LAST_COUNT:
			for (int i=rawSamples.count()-(int)range; i<rawSamples.count(); ++i)
				result.samples.append(rawSamples.at(i));
			break;
		case LOAD_NONE:
			break;
	}
	//result.samples.memdebug = false;
	//cout << "loading done!\n";
	return result;
}

Sample TimeseriesLoader::loadLast( int numInputs /*= 0*/, int numOutputs /*= 0*/, SquashType type /*= SQUASH_NONE*/, float alpha /*= 1.0f*/ )
{
	NetworkData result;
	this->type = type;
	//cout << "filling raw samples\n";
	rawSamples.clear();
	for (int i=numInputs; i<data.count();++i) {
		Sample s;
		for (int j=i-numInputs; j<i; j++) {
			s.inputs.append(data.at(j));
		}
		for (int j=i; (j<i+numOutputs); j++) {
			s.outputs.append(data.at(j));
		}
		rawSamples.append(s);
	}
	//adding LAST sample
 	Sample slast;
 	for (int j=data.count()-numInputs; j<data.count(); j++) {
 		slast.inputs.append(data.at(j));
 	}
 	for (int j=0; j<numOutputs; j++) {
 		slast.outputs.append(data.at(data.count()-1));
 	}
 	rawSamples.append(slast);

	//cout << "squashing\n";
	squash(type,alpha);
	return rawSamples.at(rawSamples.count()-1);
}

void TimeseriesLoader::setActiveInterval( int count )
{
	if (data.count() > dataFull.count()) {
		dataFull = data;
		data.clear();
		for (int i=dataFull.count()-count; i<dataFull.count(); ++i)
			data.append(dataFull[i]);
	} else {
		for (int i=dataFull.count()-count; i<dataFull.count(); ++i)
			data.append(dataFull[i]);
	}
	//active = count;
}

NetworkData TimeseriesLoader::loadPart( int numInputs /*= 0*/, int numOutputs /*= 0*/, float rangeFrom /*= 0.0f*/, float rangeTo /*= 1.0f*/, SquashType type /*= SQUASH_NONE*/, float alpha /*= 1.0f*/ )
{
	NetworkData result;
	this->type = type;
	//cout << "filling raw samples\n";
	rawSamples.clear();
	for (int i=numInputs; i<data.count();++i) { //here goes the bug if numOutputs>1
		Sample s;
		for (int j=i-numInputs; j<i; j++) {
			s.inputs.append(data.at(j));
		}
		for (int j=i; ((j<i+numOutputs) && (j<data.count())); j++) {
			s.outputs.append(data.at(j));
		}
		rawSamples.append(s);
	}
	//cout << "squashing\n";
	squash(type,alpha);
	//cout << "squash done\n";
	//result.samples.memdebug = true;
	//printf("dumping last percent: from %d to %d in %d\n", rawSamples->count()-(int)((float)rawSamples->count()*range), rawSamples->count(), rawSamples->count());
	for (int i=(int)((float)rawSamples.count()*rangeFrom); i<(int)((float)rawSamples.count()*rangeTo); ++i) {
		//printf("%d :  s=%d  a=%d\n",i, result->samples.size, result->samples.allocated);
		result.samples.append(rawSamples.at(i));
	}

	//result.samples.memdebug = false;
	//cout << "loading done!\n";
	return result;

}

