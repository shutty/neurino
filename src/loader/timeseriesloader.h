#ifndef TIMESERIES_H
#define TIMESERIES_H

#include "dataloader.h"
#include "nlib_global.h"
#include "ffile.h"

class NLIB_EXPORT TimeseriesLoader : public DataLoader
{
public:
	FList<float> data;
	TimeseriesLoader(string fileName);
	TimeseriesLoader(FList<float> data);
	virtual NetworkData load(int numInputs = 0, int numOutputs = 0, LoadType ltype = LOAD_ALL, float range = 1.0f, SquashType type = SQUASH_NONE, float alpha = 1.0f);
	virtual NetworkData loadPart(int numInputs = 0, int numOutputs = 0, float rangeFrom = 0.0f, float rangeTo = 1.0f, SquashType type = SQUASH_NONE, float alpha = 1.0f);
	Sample loadLast(int numInputs = 0, int numOutputs = 0, SquashType type = SQUASH_NONE, float alpha = 1.0f);
	virtual void setActiveInterval(int count);
	~TimeseriesLoader();
private:
	int active;
	FList<float> dataFull;
};
#endif // TIMESERIES_H
