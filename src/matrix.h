#ifndef LIST2D_H
#define LIST2D_H

// Storage class for 2D arrays
// A bit ugly, but it's functions are enough for this project tasks

//TODO:
// make all calls inlined

#include "flist.h"

template <class T>
	class Matrix 
	{

	public:
		T** data;
		int rows;
		int cols;
		Matrix(int rows, int cols);						// constructor without element init
		Matrix(int rows, int cols, T initial);			// .. with init
		Matrix();
		inline Matrix(Matrix &mat) {
			rows = mat.rows;
			cols = mat.cols;
			data = new T* [rows];
			for (int i=0; i<rows; i++) {
				data[i] = new T[cols];
				for (int j=0; j<cols; ++j) {
					data[i][j] = mat.data[i][j];
				}
			}

		}
		~Matrix();
		inline const T at(int row, int col);					// access operator
		inline void setAt(int row, int col, T value);	// write operator
		inline void init(T value);						// init all elements with a value
		inline FList<T> row(int row);
		inline FList<T> col(int col);
		inline Matrix& operator = (const Matrix& mat){
			if (this == &mat) {
				return *this;
			} else {
				if (properlyAllocated == 3.1415926f) {
					if (data!=NULL) {
						for (int i=0; i<rows; i++) {
							delete[] data[i];		// delete all elements
						}
						delete[] data;				// delete row
					}
				} else {
					int a=1;
				}
				rows = mat.rows;
				cols = mat.cols;
				data = new T* [rows];
				for (int i=0; i<rows; i++) {
					data[i] = new T[cols];
					for (int j=0; j<cols; ++j) {
						data[i][j] = mat.data[i][j];
					}
				}
				return *this;
			}
		}

	private:

		float properlyAllocated;
	};

	template <class T>
	FList<T> Matrix<T>::col( int col )
	{
		FList<T> list;
		for (int i=0; i<rows; ++i)
			list.append(data[i][col]);
		return list;
	}

	template <class T>
	FList<T> Matrix<T>::row( int row )
	{
		FList<T> list;
		for (int i=0; i<cols; ++i)
			list.append(data[row][i]);
		return list;
	}

	template <class T>
	Matrix<T>::Matrix( int rows, int cols, T initial )
	{
		this->rows = rows;
		this->cols = cols;
		properlyAllocated = 3.1415926f;
		if ((rows==0) || (cols==0))
			data = NULL;
		else {
			data = new T* [rows];
			for (int i=0; i< rows; ++i) {
				data[i] = new T[cols];		// yes, we use c++-style memory allocations
				for (int j=0; j< cols; ++j)
					data[i][j] = initial;	// element initialization
			}
		}

	}

	template <class T>
	void Matrix<T>::init( T value )
	{
		for (int i=0; i<rows; ++i)
			for (int j=0; j<cols; ++j)
				data[i][j] = value;		// element init
	}

	template <class T>
	void Matrix<T>::setAt( int row, int col, T value )
	{
		data[row][col] = value;
	}

	template <class T>
	const T Matrix<T>::at( int row, int col )
	{
		return data[row][col];
	}

	template <class T>
	Matrix<T>::~Matrix()
	{
		for (int i=0; i<rows; i++) {
			delete[] data[i];		// delete all elements
		}
		delete[] data;				// delete row
	}

	template <class T>
	Matrix<T>::Matrix( int rows, int cols )
		:cols(cols), rows(rows)
	{
		properlyAllocated = 3.1415926f;
		if ((rows==0) || (cols==0))
			data = NULL;
		else {
			data = new T* [rows];
			for (int i=0; i< rows; ++i) {
				data[i] = new T[cols];
			}
		}
	}

	template <class T>
	Matrix<T>::Matrix()
	{
		properlyAllocated = 3.1415926f;
		data = NULL;
		rows = 0;
		cols = 0;
	}

#endif // LIST2D_H
