#ifndef FHASH_H
#define FHASH_H

#include <string>
#include <iostream>
#include <fstream>
#include "fhashfunc.h"

using namespace std;

template <class Val>
class FHashElement {
public:
	bool empty;
	Val value;
	FHashElement(Val value, bool empty)
		:value(value), empty(empty) {
			//nothing here
	}
	~FHashElement() {};
	inline int hash() {
		return hash(value);
	}
private:
};

template <class Key, class Val>
class FHash {
public:
	FHash(int size=500);
	void insert(Key key, Val value);
	~FHash();
private:
	FHashElement* buffer;
};

template <class Key, class Val>
void FHash<Key, Val>::insert( Key key, Val value )
{
	FHashElement element(value,false);
}
template <class Key, class Val>
FHash<Key,Val>::~FHash()
{

}
template <class Key, class Val>
FHash<Key,Val>::FHash()
{

}
#endif // FHASH_H