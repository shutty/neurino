#ifndef NEURINO_H
#define NEURINO_H

// main header for including all library stuff into main program

#include "pugixml.hpp"

#include "elliotfunction.h"
#include "elliotsymmetricfunction.h"
#include "sigmoidfunction.h"
#include "linearfunction.h"
#include "tanhfunction.h"
#include "thresholdfunction.h"

#include "cluster.h"

#include "linker.h"
#include "linkerconvolution.h"

#include "networkdata.h"
#include "fannloader.h"
#include "probenloader.h"
#include "timeseriesloader.h"

#include "network.h"
#include "rproplearner.h"
#include "bproplearner.h"
#include "irproplearner.h"
#include "sarproplearner.h"
#include "jrproplearner.h"

#include "genetics.h"
#include "nwinit.h"


#endif // NEURINO_H