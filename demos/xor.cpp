// simplexor.cpp : 
/*
Simple XOR problem.
 1 1 = 0
 1 0 = 1
 0 1 = 1
 0 0 = 0
 XOR is a simple nonlinear function which cannot be easily interpolated via regression.
 This task clearly shows all the advantages of RPROP algo over BPROP
*/
#include <iostream>		
#include <stdio.h>
#include "neurino.h"

using namespace std;

int main(int argc, char *argv[])
{
    // Loading data
    FannLoader ldr("/home/shutty/code/neurino2/data/xor.test");
    NetworkData ndata = ldr.load();
    // Creating layers
    Cluster* layerInput = new Cluster(2, new ElliotFunction(), true, false);
    Cluster* layerHidd = new Cluster(5, new ElliotFunction(), false, false);
    Cluster* layerOut = new Cluster(1, new ElliotFunction(), false, true);
    // And moving them into the network
    Network* net = new Network();
    net->addLayer(layerInput);
    net->addLayer(layerHidd);
    net->addLayer(layerOut);
    // Linking them
    net->linkLayers(layerInput, layerHidd, new Linker());
    net->linkLayers(layerHidd, layerOut, new Linker());
    // Learning via RPROP
    RPropLearner *rprop = new RPropLearner(net);
    for (int i=0; i<100; i++) {
	// Go learn it!
	rprop->iterate(&ndata);
	if (i % 10 == 0)
	    printf("RPROP: %i \t %12f\n", i, net->getMSE(&ndata));
    }
    return(0);
}
