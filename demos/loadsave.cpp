// loadsave.cpp : 
// Robot problem from PROBEN dataset
// after loading data, learning and testing, save-load cycle is done

#include <iostream>		
#include <stdio.h>
#include "neurino.h"

int main(int argc, char *argv[])
{
    // Loading data via Fann loader
    FannLoader loaderTrain("/home/shutty/code/neurino2/data/robot.train");
    NetworkData dataTrain = loaderTrain.load();
    FannLoader loaderTest("/home/shutty/code/neurino2/data/robot.test");
    NetworkData dataTest = loaderTest.load();
    // Creating network clusters. 48-96-3 structure is used (as described in PROBEN)
    Cluster* layerInput = new Cluster(dataTrain.inputsCount(), new ElliotFunction(), true, false);
    Cluster* layerHidd = new Cluster(96, new ElliotFunction(), false, false);
    Cluster* layerOut = new Cluster(dataTrain.outputsCount(), new ElliotFunction(), false, true);
    // Creating network and adding layers to it
    Network* net = new Network();
    net->addLayer(layerInput);
    net->addLayer(layerHidd);
    net->addLayer(layerOut);
    net->linkLayers(layerInput, layerHidd, new Linker());
    net->linkLayers(layerHidd, layerOut, new Linker());
    // PugiXML xml_document is used to store network data
    xml_document* best;
    // Using RPROP algorithm
    RPropLearner rprop(net);
    float minMSE=1.0f; // Starting MSE value
    // Iterating 200 times
    for (int i=0; i<200; i++) {
	rprop.iterate(&dataTrain);
	// calculating mean square error over Test dataset
	float currMSE = net->getMSE(&dataTest);
	// print MSE over separate Train and Test dataset
	printf("\r %5d \t %14f \t %14f", i, net->getMSE(&dataTrain), currMSE);
	// later try to save the network state every iteration which minimizes error
	// this trick allows to avoid over-training
	if (currMSE<minMSE) {
	    minMSE = currMSE;
	    cout << endl;
	    // using save() method to serialize network to XML
	    best = net->save();
	}
    }
    // Creating new network from xml document to test net->xml->net conversion (case 1)
    Network nbest(best);
    // Storing XML document to file
    best->save_file("best.xml");
    // And loading in back
    xml_document xbest;
    xbest.load_file("best.xml");
    // Creating new network from xml to test net->xml->file->xml->net conversion (case 2)
    Network nbest2(&xbest);
    // Printing MSEs for original network and networks from case 1&2. MSEs should be the same nevertheless the conversion process.
    printf("\nOld: %2.7f New1: %2.7f New2: %2.7f\n", net->getMSE(&dataTest), nbest.getMSE(&dataTest), nbest2.getMSE(&dataTest));
    return 0;
}


