// robot.cpp : Defines the entry point for the console application.
//
// Более навороченный пример, посвященный поведению трехзвенного манипулятора
// на основании внешних данных. Входы (48 штук)- значения внешних сенсоров, выходы (3 штуки) - 
// - значения углов поворота сервомоторов манипулятора
// Данные в формате fann, описание их дано в примере simplexor

// В этом примере хорошо виден метод, которым можно вычислить оптимальное значение количества итераций.

#include <iostream>		
#include <stdio.h>
#include "neurino.h"	// Включаем заголовок-сборку всех относящихся к сети классов

int main(int argc, char *argv[])
{
	NetworkData* dataTrain = new NetworkData();
	dataTrain->loadFannData("../bench/robot.train");
	NetworkData* dataTest = new NetworkData();
	dataTest->loadFannData("../bench/robot.test");
	Cluster* layerInput = new Cluster(dataTrain->inputsCount(), new ElliotFunction(), true, false);
	// Значение в 96 нейронов в скрытом слое для этой задачи я нашел в какой-то книжке
	// говорят - оптимальное значение. 
	Cluster* layerHidd = new Cluster(96, new ElliotFunction(), false, false);
	Cluster* layerOut = new Cluster(dataTrain->outputsCount(), new ElliotFunction(), false, true);
	Network* net = new Network();
	net->addLayer(layerInput);
	net->addLayer(layerHidd);
	net->addLayer(layerOut);
	net->linkLayers(layerInput, layerHidd, new Linker());
	net->linkLayers(layerHidd, layerOut, new Linker());
	SARPropLearner rprop(net,0.041f);
	// Пытаемся вычислить оптимальное количество итераций для обучения, чтобы
	// сеть показывала максимальные способности к обобщению данных, которые она до этого
	// момента вообще не видела
	float minMSE=1.0f;
	for (int i=0; i<200; i++) {
		rprop.iterate(dataTrain);
		// считаем среднеквадратичную ошибку по проверочным данным
		float currMSE = net->getMSE(dataTest);
		// красиво пишем текущий статус. \r - это возврат каретки.
		printf("\r %5d \t %10f \t %10f", i, net->getMSE(dataTrain), currMSE);
		if (currMSE<minMSE) {
			minMSE = currMSE;
			// если текущая СКО меньше предыдущей, то запоминаем ее и печатаем.
			cout << endl;
		}
	}
	return 0;
}

