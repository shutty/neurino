// cancer_genetics.cpp : 
/*

  Cancer patients classification problem.
  The most interesting part of thos problem is that according to original research, it requires
  network with 2 hidden layers (usually 9-4-2-2 structure)

  Here we'll try to prove it, dynamically adjusting network structure.

  Genetics solver has a lot of parameters, so please refer to documentation.

*/


#include <iostream>		
#include <stdio.h>
#include "neurino.h"

int main(int argc, char *argv[]) {
    // Loading data. Here single loader is used to extract two portions of data
    ProbenLoader ldr("/home/shutty/code/neurino2/data/cancer2.dt");
    ldr.setLoadingInterval(ProbenLoader::PROBEN_TRAIN); // train interval
    NetworkData dataTrain = ldr.load();
    //dataTrain->loadProbenData("../bench/cancer2.dt",PROBEN_TRAIN);
    ldr.setLoadingInterval(ProbenLoader::PROBEN_VALIDATE); // validation interval
    NetworkData dataTest = ldr.load();
    // Here is the "Adam" structure is defined, e.g. the first gene
    FList<int> sizes;
    sizes.append(dataTrain.inputsCount()); // 9-3-3-2 structure
    sizes.append(3);
    sizes.append(3);
    sizes.append(dataTrain.outputsCount());
    // creating initial gene from the structure
    Gene* initial = new Gene(sizes);
    // creating genetic algo class with these parameters:
    // 2..5 - cluster size range
    // 0.1f - alpha population amount. these alphas have more chanses to spare
    // 0.4f - omega population count. These omegas are the outsiders of evolution
    // 15..20 - population size (genes)
    // 20 generations
    // 10 tests for each gene in population, to minimize error
    // 300 - iteration limit
    Genetics* genetics = new Genetics(dataTrain, dataTest, initial, 2, 5, 0.1f, 0.4f, 15, 20, 20, 10, 300);
    // running genetics algo. It should print some statistical data.
    Gene best = genetics->populate();
    // creating network using the best gene
    Network *net = new Network(&best, new ElliotFunction());
    //  and printing the result
    net->dumpNetworkInfo();
    //	Gene
}
