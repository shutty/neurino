#include <iostream>		
#include <stdio.h>
#include "neurino.h"	// Включаем заголовок-сборку всех относящихся к сети классов

int main(int argc, char *argv[])
{
	NetworkData* dataTrain = new NetworkData();
	dataTrain->loadFannData("../bench/robot.train");
	NetworkData* dataTest = new NetworkData();
	dataTest->loadFannData("../bench/robot.test");
	FList<float> errors;
	FList<int> indexes;
	for (int t=0; t<30; ++t) {
		Cluster* layerInput = new Cluster(dataTrain->inputsCount(), new ElliotFunction(), true, false);
		Cluster* layerHidd = new Cluster(96, new ElliotFunction(), false, false);
		Cluster* layerOut = new Cluster(dataTrain->outputsCount(), new ElliotFunction(), false, true);
		Network* net = new Network();
		net->addLayer(layerInput);
		net->addLayer(layerHidd);
		net->addLayer(layerOut);
		net->linkLayers(layerInput, layerHidd, new Linker());
		net->linkLayers(layerHidd, layerOut, new Linker());
		//NWInit(net, dataTrain);
		RPropLearner rprop(net);
		float minMSE=1.0f;
		int minIt = 0;
		for (int i=0; i<100; i++) {
			rprop.iterate(dataTrain);
			// считаем среднеквадратичную ошибку по проверочным данным
			float currMSE = net->getMSE(dataTest);
			// красиво пишем текущий статус. \r - это возврат каретки.
			printf("\r %5d \t %10f \t %10f", i, net->getMSE(dataTrain), currMSE);
			if (currMSE<minMSE) {
				minMSE = currMSE;
				minIt = i;
				// если текущая СКО меньше предыдущей, то запоминаем ее и печатаем.
				cout << endl;
			}
		}
		errors.append(minMSE);
		indexes.append(minIt);
		delete net;
	}
	printf("\n\nmse: %2.7f  %2.7f \n", average(errors), dispersion(errors));
	printf(" it: %2.7f  %2.7f \n", average(indexes), dispersion(indexes));
	return 0;
}
