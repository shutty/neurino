#include "neurino.h"
#include <iostream>

/*
  Perf metrics for neural network. Robot problem was used from PROBEN dataset.
  This example uses rdtsc x86 operation, so it may not compile on non-x86 hardware.

  Useful for testing compiler options. As for GCC, difference between -O2 and "-O3 -ffash-math -fomit-frame-pointer -march=native"
  is usually not more than 5-7%. For ICC - 10% (tested on Celeron ULV 723 laptop).

  */

static inline unsigned long long rdtsc() {
    unsigned long long d;
    __asm__ __volatile__ ("rdtsc" : "=A" (d) );
    return d;
}

int main(int argc, char** argv) {
    // Usual stuff for NN init...
    FannLoader ldrTrain("/home/shutty/code/neurino2/data/robot.train");
    FannLoader ldrTest("/home/shutty/code/neurino2/data/robot.test");
    NetworkData dataTrain = ldrTrain.load();
    NetworkData dataTest = ldrTest.load();
    Cluster* layerInput = new Cluster(dataTrain.inputsCount(), new ElliotFunction(), true, false);
    Cluster* layerHidd = new Cluster(40, new ElliotFunction(), false, false);
    Cluster* layerOut = new Cluster(dataTrain.outputsCount(), new ElliotFunction(), false, true);
    Network* net = new Network();
    net->addLayer(layerInput);
    net->addLayer(layerHidd);
    net->addLayer(layerOut);
    net->linkLayers(layerInput, layerHidd, new Linker());
    net->linkLayers(layerHidd, layerOut, new Linker());
    RPropLearner rprop(net);
    FList<float> perf;
    for (int i=0; i<100; ++i) {
	// calling rdtsc before and after network iteration
	unsigned long int before = rdtsc();
	rprop.iterate(&dataTrain);
	unsigned long int after = rdtsc();
	float res = (float)(after-before)/1000000.0f; // so we got number of millions of operations per single iteration
	perf.append(res);
	printf("%4d: %8.6f %8.6f %8.6f\n",i,net->getNRMS(&dataTrain), net->getNRMS(&dataTest), res);
    }
    // average(), median() and dispersion() functions are defined in <minimath.h>
    // I'm not using STL/boost because of dependency and memory footprint minimization
    printf("Perfomance: %8.6f %8.6f %8.6f\n",average(perf), median(perf),  dispersion(perf));
    return 0;
}
