// advancedxor.cpp : 
/*
Bit xor problem. The problem is the same as in xor.cpp, but with some more advanced data loading and multiple learn algorithms
*/
#include <iostream>		
#include <stdio.h>
#include "neurino.h"

using namespace std;

int main(int argc, char *argv[])
{
    // Creating network clusters. For a XOR problem, three-layered network is enough.
    // 2-3-1 structure was selected empirically. 2-2-1 or 2-10-1 is fine too.
    Cluster* layerInput = new Cluster(2, new ElliotFunction(), true, false);
    Cluster* layerHidd = new Cluster(3, new ElliotFunction(), false, false);
    Cluster* layerOut = new Cluster(1, new ElliotFunction(), false, true);
    // Creating the network. It is empty by default
    Network* net = new Network();
    // Adding clusters to it
    net->addLayer(layerInput);
    net->addLayer(layerHidd);
    net->addLayer(layerOut);
    // Linking them. Theoretically you can also create more complex structures, but usually it is inefficient
    net->linkLayers(layerInput, layerHidd, new Linker());
    net->linkLayers(layerHidd, layerOut, new Linker());
    // Creating copy of the network
    Network *net2(net);
    // The most interesting part:
    // Manually creating 4 samples
    Sample s1,s2,s3,s4;
    s1.inputs.append(0);
    s1.inputs.append(0);
    s1.outputs.append(0); // 0 xor 0 = 0

    s2.inputs.append(0);
    s2.inputs.append(1);
    s2.outputs.append(1); // 0 xor 1 = 1

    s3.inputs.append(1);
    s3.inputs.append(0);
    s3.outputs.append(1); // 1 xor 0 = 1

    s4.inputs.append(1);
    s4.inputs.append(1);
    s4.outputs.append(0); // 1 xor 1 = 0

    // Creating data storage for our newly-created samples
    NetworkData data;
    // and adding samples
    data.samples.append(s1);
    data.samples.append(s2);
    data.samples.append(s3);
    data.samples.append(s4);


    // Creating RPROP & BPROP learner
    RPropLearner *rprop = new RPropLearner(net);
    BPropLearner *bprop = new BPropLearner(net2);
    // And learning for 5k iterations
    for (int i=0; i<500000; i++) {
	// make ineration
	rprop->iterate(&data);
	bprop->iterate(&data);
	if (i % 10000 == 0)
	    // and print error for current step
	    cout << i << "\t" << net->getMSE(&data) << "\t" << net2->getMSE(&data) << endl;
    }
    // Here the network is successfully learned.
    // Testing all samples
    FList<float> out1 = net->testSample(s1.inputs);
    FList<float> out2 = net->testSample(s2.inputs);
    FList<float> out3 = net->testSample(s3.inputs);
    FList<float> out4 = net->testSample(s4.inputs);
    // Printing the result
    printf("%2.4f xor %2.4f = %2.4f\n", s1.inputs[0], s1.inputs[1], out1[0]);
    printf("%2.4f xor %2.4f = %2.4f\n", s2.inputs[0], s2.inputs[1], out2[0]);
    printf("%2.4f xor %2.4f = %2.4f\n", s3.inputs[0], s3.inputs[1], out3[0]);
    printf("%2.4f xor %2.4f = %2.4f\n", s4.inputs[0], s4.inputs[1], out4[0]);
    return(0);
}
