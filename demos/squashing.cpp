// squashing.cpp :
//
// Задача "таблица умножения"
// Во входном файле - таблица умножения 0..3
// Прелесть в том, что все эти данные сначала умнутся в диапазон 0..1
// а во время тестирования будут незаметно разворачиваться обратно

#include <iostream>		
#include <stdio.h>
#include "neurino.h"	// Включаем заголовок-сборку всех относящихся к сети классов

void printSample(Sample* s) {
	printf("%8.6f * %8.6f = %8.6f\n", s->inputs[0], s->inputs[1], s->outputs[0]);
}

int main(int argc, char* argv[])
{
	FannLoader ldr("../bench/multiply.test");
	NetworkData data = ldr.load(0,0,DataLoader::LOAD_ALL, 1.0f, DataLoader::SQUASH_LINEAR,1.0f);
//	NetworkData* data = new NetworkData();
	// загружаем данные
	//data->loadFannData("../bench/multiply.test");
	// и линейно их уминаем
	//data->squash(NetworkData::SQUASH_LINEXP,2.0f);

	Cluster* layerInput = new Cluster(2, new ElliotFunction(), true, false);
	Cluster* layerHidd = new Cluster(8, new ElliotFunction(), false, false);
	Cluster* layerOut = new Cluster(1, new ElliotFunction(), false, true);
	// Создаем саму сеть. 
	Network* net = new Network();
	// Добавляем кластеры в сеть
	net->addLayer(layerInput);
	net->addLayer(layerHidd);
	net->addLayer(layerOut);
	// И связываем их друг с другом. 
	net->linkLayers(layerInput, layerHidd, new Linker());
	net->linkLayers(layerHidd, layerOut, new Linker());
	// Создаем класс-учитель
	RPropLearner *rprop = new RPropLearner(net);
	for (int i=0; i<5000; i++) {
		rprop->iterate(&data);
		if (i % 1000 == 0)
			cout <<i << ": " << net->getMSE(&data,true) << endl; // к слову о птичках: ошибка считается по сплющенным данным!
	}
	cout << endl;
	for (int i=0; i<data.samples.count();++i)
		// печатаем развернутые обратно данные, предварительно их оттестировав
		printSample(ldr.desquash(net->testSample(data.samples[i])));
	return 0;
}

