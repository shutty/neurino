#ifndef LIST2D_H
#define LIST2D_H

template <class T>
	class Matrix 
	{

	public:
		int rows;
		int cols;
		Matrix(int rows, int cols);
		Matrix(int rows, int cols, T initial);
		~Matrix();
		inline T at(int row, int col);
		inline void setAt(int row, int col, T value);
		inline void init(T value);

	private:
		T** data;
		
	};

	template <class T>
	Matrix<T>::Matrix( int rows, int cols, T initial )
	{
		data = new T* [rows];
		for (int i=0; i< rows; ++i) {
			data[i] = new T[cols];
			for (int j=0; j< cols; ++j)
				data[i][j] = initial;
		}

	}

	template <class T>
	void Matrix<T>::init( T value )
	{
		for (int i=0; i<rows; ++i)
			for (int j=0; j<cols; ++j)
				data[i][j] = value;
	}

	template <class T>
	void Matrix<T>::setAt( int row, int col, T value )
	{
		data[row][col] = value;
	}

	template <class T>
	T Matrix<T>::at( int row, int col )
	{
		return data[row][col];
	}

	template <class T>
	Matrix<T>::~Matrix()
	{
		for (int i=0; i<rows; i++) {
			delete[] data[i];
		}
		delete[] data;
	}

	template <class T>
	Matrix<T>::Matrix( int rows, int cols )
		:cols(cols), rows(rows)
	{
		data = new T* [rows];
		for (int i=0; i< rows; ++i) {
			data[i] = new T[cols];
		}
	}
#endif // LIST2D_H
